package scb

class BootStrap {

    def init = { servletContext ->
        boolean initResult = true
        initResult &= buildConfigs()
        initResult &= buildObjects()
        if(initResult == false){
            println "\n\nAPPLICATION STARTUP GOT FAILED"
            System.exit(-1)
        }else{
            println "\n\nAPPLICATION STARTUP SUCCESS"
        }
    }


    def buildConfigs() {
        boolean result = true
        List smsConfigList = [            
            // admin configs
            new Config(key: "masterSessionId", value:"-"),
            new Config(key: "masterPassword", value:"admin"),
            new Config(key: "imageMaxSize", value: "2097152"),
            new Config(key: "adminContactNumber", value: "+91 77088 07996"),
            new Config(key: "adminEmailAddress", value: "srichellasbakery@gmail.com"),
            new Config(key: "whatsappOrderNotification", value: "919488592710,917708807996"),
            new Config(key: "telegramOrderNotification", value: "-1001712055306"),
            new Config(key: "telegramBotId", value: "5408398417:AAGg_5ffTV7PHrYWYNSwMRBFcBil19-0Ar0"),

            new Config(key: "razorpayMode", value: "Test"),
            new Config(key: "razorpayKeyIdTest", value: "rzp_test_yvinhFVJcimk6r"),
            new Config(key: "razorpayKeySecretTest", value: "QFF3oIlIVnDwRwmXvDYSvgA2"),
            new Config(key: "razorpayKeyAuthtokenTest", value: "cnpwX3Rlc3RfeXZpbmhGVkpjaW1rNnI6UUZGM29JbElWbkR3UndtWHZEWVN2Z0Ey"),            
            new Config(key: "razorpayKeyIdLive", value: "rzp_live_OQsDTcqKIKCdwA"),
            new Config(key: "razorpayKeySecretLive", value: "g1YUKdP0NzUhrArYXb5Oo7c7"),
            new Config(key: "razorpayKeyAuthtokenLive", value: "cnpwX2xpdmVfT1FzRFRjcUtJS0Nkd0E6ZzFZVUtkUDBOelVockFyWVhiNU9vN2M3"),            


            new Config(key: "facebookUrl", value: "https://www.facebook.com/ChellasBakery"),            
            new Config(key: "whatsappMsgUrl", value: "https://wa.me/+917708807996"),
            new Config(key: "twitterUrl", value: "https://www.twitter.com"),
            new Config(key: "instagramUrl", value: "https://www.instagram.com/chellas_bakery/"),
            new Config(key: "youtubeUrl", value: "https://www.youtube.com"),

            new Config(key: "experienceValue", value: "50"),
            new Config(key: "cakeValue", value: "500"),
            new Config(key: "staffsValue", value: "50"),
            new Config(key: "customerValue", value: "5000"),
            new Config(key: "websiteHit", value: "0"),
            new Config(key: "websiteStoryIntro", value:"The amount of love gained by you people is just pure and we are keep on improving our services and tastes."),
            new Config(key: "websiteStoryFooter", value:"We serve and take all type of cake orders. You can personalised cake of your own choice. We have the best chef in the city working for us and happying serving to you."),
            new Config(key: "companyName", value:"Sri Chellas Bakery and Sweets"),
            new Config(key: "aboutStory1", value:"Started from the parental organization of Thangam paal pannai in 1970s."),
            new Config(key: "aboutStory2", value:"Main Branch Located in Sivakasi near Ratna Vilas Bus Stop"),
            new Config(key: "aboutStory3", value:"Other Branches - near Sivan koil and Mariamman koil"),
            new Config(key: "aboutMainContent", value: "Welcome to our online store, where a world of pure Ghee Sweets, Cashew wonders, divine Paal varieties, and Moru Moru Kaaras wait to fill your homes with love, light & joy. Delivering happiness, since 1980s & beyond."),
            new Config(key: "companyAddress", value:"Bypass Rd, Rathina Vilas Bus Stop, Sivakasi, Tamil Nadu 626123"),
            new Config(key: "branchAddress1", value:"FQ2W+3GV, Sivan Sannathi Road, Parasakthi Colony, Sivakasi, Tamil Nadu 626123")            
        ]
        smsConfigList.each{
            if(Config.findByKey(it.key) == null){
                it.isMandatory = true
                result &= it.save() != null
            }
        }
        return result
    }

    def buildObjects = {
        boolean result = true
        List objectList = [
            new Category(name: "Wedding cakes", description : "-"),           
            new Category(name: "Party cakes", description : "-"),           
            new Category(name: "Engagement cakes", description : "-"),          
            new Category(name: "Birthday cakes", description : "-"),       
            new Category(name: "Breakup cakes", description : "-"),     
            new Category(name: "New year cakes", description : "-")                       
        ] 
        // objectList.each{
        //     if(Category.findByName(it.name) == null){
                
        //         result &= it.save() != null
        //     }
        // }
        List cakeobjectList = [
            new Product(name: "Choco cake", price:230,description : "description about the cake which has price and kg available" ,category:Category.findByName("Wedding cakes")),
            new Product(name: "Red velvet cake", price:330,description : "description about the cake which has price and kg available",category:Category.findByName("Wedding cakes")),
            new Product(name: "Black velvet cake", price:430,description : "description about the cake which has price and kg available",category:Category.findByName("Wedding cakes")),
            new Product(name: "Green velvet cake", price:530,description : "description about the cake which has price and kg available",category:Category.findByName("Wedding cakes")),
            new Product(name: "Blue velvet cake", price:630,description : "description about the cake which has price and kg available",category:Category.findByName("Wedding cakes")),
            new Product(name: "Orange velvet cake", price:730,description : "description about the cake which has price and kg available",category:Category.findByName("Wedding cakes")),

            new Product(name: "Pineapple cake", price:200,description : "description about the cake which has price and kg available",category:Category.findByName("Party cakes")) ,       
            new Product(name: "Grapes cake", price:400,description : "description about the cake which has price and kg available",category:Category.findByName("Party cakes")) ,       
            new Product(name: "Orange cake", price:500,description : "description about the cake which has price and kg available",category:Category.findByName("Party cakes")) ,

            new Product(name: "Biscuit cake", price:600,description : "description about the cake which has price and kg available",category:Category.findByName("Engagement cakes")),
            new Product(name: "Popcorn cake", price:700,description : "description about the cake which has price and kg available",category:Category.findByName("Engagement cakes")),
            new Product(name: "Idli cake", price:330,description : "description about the cake which has price and kg available",category:Category.findByName("Engagement cakes")),
            new Product(name: "Dosa cake", price:630,description : "description about the cake which has price and kg available",category:Category.findByName("Engagement cakes")),

            new Product(name: "Carrot cake", price:930,description : "description about the cake which has price and kg available",category:Category.findByName("Birthday cakes")),
            new Product(name: "Sweet cake", price:530,description : "description about the cake which has price and kg available",category:Category.findByName("Birthday cakes")),
            new Product(name: "Honey cake", price:830,description : "description about the cake which has price and kg available",category:Category.findByName("Birthday cakes")),
            new Product(name: "Number cake", price:930,description : "description about the cake which has price and kg available",category:Category.findByName("Birthday cakes")),
            new Product(name: "Pattern cake", price:230,description : "description about the cake which has price and kg available",category:Category.findByName("Birthday cakes")),


            new Product(name: "Heart cake", price:730,description : "description about the cake which has price and kg available",category:Category.findByName("Breakup cakes")),
            new Product(name: "Kitkat cake", price:930,description : "description about the cake which has price and kg available",category:Category.findByName("Breakup cakes")),

            new Product(name: "Black forest cake", price:630,description : "description about the cake which has price and kg available",category:Category.findByName("New year cakes")),
            new Product(name: "White forest cake", price:430,description : "description about the cake which has price and kg available",category:Category.findByName("New year cakes"))
        ] 
        
        // cakeobjectList.each{
        //     if(Product.findByName(it.name) == null){                
        //         result &= it.save() != null
        //     }
        // }

        List imageobjectList = [
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Choco cake").id,fileName:"2elLd3kemP.jpg",urlpath : null,imageBytes :null,imageContentType:null),    
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Red velvet cake").id,fileName:"6HNmcGLFLh.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Black velvet cake").id,fileName:"AXfW3yUbdw.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Green velvet cake").id,fileName:"bhqdEYGi3V.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Blue velvet cake").id,fileName:"F3RkQpj2yo.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Orange velvet cake").id,fileName:"JjrTTc6sFa.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Pineapple cake").id,fileName:"SWvloyniMS.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Grapes cake").id,fileName:"VWrlw70D7Y.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Orange cake").id,fileName:"WEdHDujMT4.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Biscuit cake").id,fileName:"zlvWsGwE5F.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Popcorn cake").id,fileName:"insta_image_1.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Idli cake").id,fileName:"insta_image_2.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Dosa cake").id,fileName:"insta_image_3.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Carrot cake").id,fileName:"insta_image_4.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Sweet cake").id,fileName:"insta_image_5.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Honey cake").id,fileName:"insta_image_6.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Number cake").id,fileName:"menu1.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Pattern cake").id,fileName:"menu2.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Heart cake").id,fileName:"menu3.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Kitkat cake").id,fileName:"menu4.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("Black forest cake").id,fileName:"menu5.jpg",urlpath : null,imageBytes :null,imageContentType:null),
            // new Image(source : ImageSource.STATIC_URL,entity:ImageEntity.PRODUCT,entityId:Product.findByName("White forest cake").id,fileName:"menu7.jpg",urlpath : null,imageBytes :null,imageContentType:null)
        ] 
        
        // imageobjectList.each{
        //     try{
        //         if(Image.findByFileName(it.fileName) == null){            
        //         result &= it.save() != null
        //         }    
        //     }catch(Exception e){
        //         println(e);
        //     }        
        // }
        List productTagList = [
            new ProductTag(name: "BestSeller"),           
            new ProductTag(name: "Premium"),           
            new ProductTag(name: "NewArrival")                     
        ] 
        productTagList.each{
            if(ProductTag.findByName(it.name) == null){                
                result &= it.save() != null
            }
        }
        return result
    }

    def destroy = {
    }
}
