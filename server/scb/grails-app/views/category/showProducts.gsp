<html>
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700" rel="stylesheet" type="text/css">
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">		

		<asset:javascript src="index.js"/>
		<asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>		
		<asset:stylesheet src="showProducts.css"/>
		
		<g:set var="servepath" value="${grailsApplication.config.getProperty('grails.var.servepath')}"/>
		<g:set var="savepath" value="${grailsApplication.config.getProperty('grails.var.savepath')}"/>

		<g:set var="configs" value="${scb.Config.list()}" />
		<script>var whatsappMsgUrl = "${configs.find{it.key =='whatsappMsgUrl'}.value}";</script>
        <style>.hidden{display: none;}</style>
	</head>

<body>	
	<div class="text-center" style="margin: 20px;">
	    <span class="subheading subheading-special">${categoryName}</span>
	</div>
    <div class="sort-filter-dropdown"> 
        <b><label class="sortby">Sort By</label></b>
	   <!-- <div class="select-wrap one-third">  -->
            <!-- <div class="icon"><span class="ios-ios-arrow-down"></span></div> -->
            <select required id="sortby" style="width:200px!important;
             margin: 0 10px!important;" class="form-control" onchange="sortBy()">
                <option value="all">All</option>    
                <option value="bestseller">Best Seller</option>    
                <option value="newarrival">New Arrival</option>
                <option value="premium">Premium</option>               
            </select>
        <!-- </div> -->
    </div>
	<!-- iterate the product list -->
	<section class="ftco-section" id="menu">
        <div class="container-fluid px-4">
			<div id="productList" class="row" style="width: 100%;margin: 0px;">
			        <g:each in="${products}">
			            <g:set var="imgList" value="${scb.Image.findAllByEntityAndEntityId(scb.ImageEntity.PRODUCT,it.id).reverse()}" />
			            <g:set var="img" value="${imgList.find{new File(savepath+it.fileName).exists() == true}}" />
			            <g:if test="${img != null}">
			                <div class="menus d-flex col-12 p-0 mobile-menu proList">
                                <g:if test="${it.productTag?.name == 'BestSeller'}">
                                    <span class="bestsellerbadge wave-effect">Best Seller</span>
                                </g:if>
                                <g:if test="${it.productTag?.name == 'Premium'}">
                                    <span class="premiumbadge wave-effect">Premium</span>
                                </g:if>                                    
                                <g:if test="${it.productTag?.name == 'NewArrival'}">
                                    <span class="newarrivalbadge wave-effect">New Arrival</span>
                                </g:if>                                    
			                    <asset:image class="menu-img img" src="${servepath+img.fileName}" alt="" style="width: unset;height: 220px;object-fit: scale-down;"/>
			                    <div class="text">
			                        <div class="d-flex">
			                            <div class="one-half">
			                                <div class="descFontWeight" title=${it.name}>${it.description}			     	
			                                </div>
			                            </div>
			                            <div class="one-forth">
			                                <span class="price">₹${it.price}</span>
			                            </div>
			                        </div>
			                        <!-- <p><span>${it.description}</span></p> -->
			                        <div style="text-align:center;">
			                         <button class="button" id="trigger" onclick="openModal('${it.id}', '${it.name}','${it.price}','','${servepath+img.fileName}','${it.standardKg}')"><span>Order</span></button>
			                        </div>
			                    </div>
			                </div>
			            </g:if>
			        </g:each>
                    <div id="noResultsDiv" class="dN">                    
                        <asset:image src="notfound.png" class="notfound"/>
                        <span>No results found</span>
                    </div>
				</div>
			</div>
	</section>

	<!-- order a cake popup-->
    <div id="freezeBackground" class="freezeBackground">
    <div id="orderCake" class="dN leaveMsg">            
            <div class="newInputStyleEle clearFix">
                    <div>
                        <h4><span id="updtitle" class="formTitle vam">Order your product</<span>
                        <button id="closePopup" class="close" onclick="closeModal()">&times;</button>
                        <h4>
                    </div>                  
                    <br/>
                    <div class="row">                            
                            <div class="col-md-6 dN">
                                <h6>
                                    <b>Product Name : </b>
                                    <span class="red" id="pname"></span> 
                                </h6>                           
                            </div>
                            <div class="col-md-6">
                                <h6>
                                    <b>Price : </b>
                                    <span class="red" id="pprice"></span>
                                    <span id="ppricekg" class="hidden"> / Kg</span>
                                </h6>
                            </div>                        
                            <!-- </br>
                            <div class="col-md-6">
                                 <b>Description : </b>
                                 <span id="pdesc"></span>                        
                            </div>  -->                       
                    </div>
                     </br>
                    <div class="fL">                        
                        <form onsubmit="OrderProduct(event)">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b><label for="">Name</label></b>
                                    <input id="cusName" type="text" required class="form-control" placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b><label for="">Delivery Date</label></b>
                                    <input id="cusDate" type="date" minDate:2022-03-17  required class="form-control" id="book_date" placeholder="Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b><label for="">Phone</label></b>
                                    <input id="cusPhone" type="tel" maxlength="10" 
                                    onkeypress="return onlyNumber('event')" required class="form-control" placeholder="Eg: 9876543210">
                                    <span id="phoneError" class="dN colorRed">Invalid Phone Number</span>
                                </div>
                            </div>                                                       
                            <div class="col-md-6">
                                <div class="form-group">
                                   <b> <label for="">Delivery Time</label></b>
                                    <input id="cusTime" type="time" required class="ddd form-control" id="book_time" placeholder="Time">
                                    <span id="timeError" class="dN colorRed">Choose between 6.00am to 10.00pm</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b><label for="">Address</label></b>       
                                    <textarea required id="cusAddr" class="form-control"  rows="2" cols="2" placeholder="Enter your address"></textarea>
                                </div>
                            </div> 
                             <div class="col-md-6">
                                <div class="form-group">
                                   <b><label for="">Cake Message</label></b>
                                    <textarea required id="cusMessage" class="form-control"  rows="2" cols="2" placeholder="Enter your message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b><label for="">Email</label></b>
                                    <input id="cusEmail" type="email" required class="form-control" placeholder="Eg: xyz@gmail.com">
                                    <span id="emailError" class="dN colorRed">Invalid Email Address</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b><label for="">Kg</label></b>
                                    <div id="kgdiv" class="dN select-wrap one-third">
                                        <div class="icon"><span class="ios-ios-arrow-down"></span></div>
                                        <select required id="Kg" class="form-control" onchange="kgChange()">
                                            <option value="custom">Custom</option>    
                                            <!-- <option value="0.5">1/2 kg</option> -->
                                            <option value="1">1 kg</option>
                                            <option value="1.5">1.5 kg</option>
                                            <option value="2">2 kg</option>
                                            <option value="2.5">2.5 kg</option>
                                            <option value="3">3 kg</option>
                                            <option value="3.5">3.5 kg</option>
                                            <option value="4">4 kg</option>
                                            <option value="4.5">4.5 kg</option>
                                            <option value="5">5 kg</option>
                                        </select>
                                    </div>
                                     <input id="standardkg" type="text" disabled class="dN form-control">
                                </div>
                            </div>   
                             <div id="customKgDiv" class="col-md-6 dN">
                                <div class="form-group">
                                    <b><label for="">Custom Kg</label></b>
                                     <input id="cusKg" type="text" class="form-control" placeholder="Enter your Custom Kg" onkeypress="return onCustomKgChange('event')">
                                     <span id="customKgError" class="dN colorRed">Please enter a value</span>
                                </div>                                
                            </div>
                            <div class="col-md-12 mt-3">
                                <div class="form-group text-center">
                                    <input type="submit" id="submit-button" class="orderAndpPayNowBtn" value="Order & Pay Now">
                                </div>
                            </div>
                            <!-- <div class="col-md-12 mt-3">
                                <div class="form-group text-center">
                                    <h6 id="success-message"></h6>
                                </div>
                            </div> -->
                        </div>
                    </form>
                </div>                   
            </div>                
    </div>
    </div>
    
    <div id="freezelayer" class="freezeBackgroundPopBtn"></div>
    <div id="orderSuccessPop" class="dN orderSuccessPop">
        <p id="success-message"></p>
            <div style="text-align:center;">                
                <button id="orderSuccessPopBtn" onclick ="closeOrderSuccessPop()" class="orderSuccessPopBtn">            
                    <span id="invoiceSpan"></span>
                </button>
            </div>
    </div>   


    <div id="invoiceInfo" class="invoice-box dN">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                        <td class="title">
                            <asset:image src="logo.png" style="width: 200px; height: 50px;"/>
                        </td>
                        <td>                            
                            <span> Invoice Id : </span>
                            <span id="invoiceId"></span><br/>                           
                            <span> Invoice Date : </span>
                            <span id="invoiceDate"></span><br/> 
                            <span> Invoice Time : </span>
                            <span id="invoiceTime"></span><br/>                            
                        </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <span> Address : </span>
                                <span id="invoiceAddress"></span><br/>  
                                <span> Contact No : </span>
                                <span id="invoiceContactNo"></span><br/>
                            </td>
                            <td>
                                <span> Name : </span>
                                <span id="invoiceName"></span><br/> 
                                <span> Email : </span>
                                <span id="invoiceEmail"></span><br/>    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


             <tr class="heading">
                <td>Order Info</td><td></td>             
                <td>Values</td><td></td>             
            </tr>
            <tr class="details">
                <td>Order Date</td><td></td>
                <td id="invoiceOrderDate"></td><td></td>
            </tr>
            <tr class="details">
                <td>Order Time</td><td></td>
                <td id="invoiceOrderTime"></td><td></td>
            </tr>
            <tr class="details">
                <td>Cake Message</td><td></td>
                <td id="invoiceCakeMessage"></td><td></td>
            </tr>

            <tr class="heading">
                <td>Product</td>                
                <td>Quantity (Kg)</td>
                <td>Rate/Kg</td>
                <td>Price</td>
            </tr>
            <tr class="item">
                <td id="invoiceProductName"></td>
                <td id="invoiceProductQuantity"></td>
                <td id="invoiceProductRatePerKg"></td>
                <td id="invoiceProductPrice"></td>
            </tr>        
            <tr class="item"> 
                <td></td>
                <td></td>
                <td></td>
                <td>           
                    <span> Total : </span>
                    <span id="invoiceProductTotalPrice"></span>                 
                </td>
            </tr>

        </table>

        <br/> <br/>
        <p class="text-center red">
            Thanks for ordering with us, Have a nice day!
        </p>

        <div class="footer-copyright">
            <span>© 2022, ${configs.find{it.key =='companyName'}.value}. All Rights Reserved.</span>
        </div>
        <ul class="payment-icon-list">          
                <li><svg class="payment-icon" xmlns="http://www.w3.org/2000/svg" role="img" viewBox="0 0 38 24" width="38" height="24" aria-labelledby="pi-american_express"><title id="pi-american_express">American Express</title><g fill="none"><path fill="#000" d="M35,0 L3,0 C1.3,0 0,1.3 0,3 L0,21 C0,22.7 1.4,24 3,24 L35,24 C36.7,24 38,22.7 38,21 L38,3 C38,1.3 36.6,0 35,0 Z" opacity=".07"></path><path fill="#006FCF" d="M35,1 C36.1,1 37,1.9 37,3 L37,21 C37,22.1 36.1,23 35,23 L3,23 C1.9,23 1,22.1 1,21 L1,3 C1,1.9 1.9,1 3,1 L35,1"></path><path fill="#FFF" d="M8.971,10.268 L9.745,12.144 L8.203,12.144 L8.971,10.268 Z M25.046,10.346 L22.069,10.346 L22.069,11.173 L24.998,11.173 L24.998,12.412 L22.075,12.412 L22.075,13.334 L25.052,13.334 L25.052,14.073 L27.129,11.828 L25.052,9.488 L25.046,10.346 L25.046,10.346 Z M10.983,8.006 L14.978,8.006 L15.865,9.941 L16.687,8 L27.057,8 L28.135,9.19 L29.25,8 L34.013,8 L30.494,11.852 L33.977,15.68 L29.143,15.68 L28.065,14.49 L26.94,15.68 L10.03,15.68 L9.536,14.49 L8.406,14.49 L7.911,15.68 L4,15.68 L7.286,8 L10.716,8 L10.983,8.006 Z M19.646,9.084 L17.407,9.084 L15.907,12.62 L14.282,9.084 L12.06,9.084 L12.06,13.894 L10,9.084 L8.007,9.084 L5.625,14.596 L7.18,14.596 L7.674,13.406 L10.27,13.406 L10.764,14.596 L13.484,14.596 L13.484,10.661 L15.235,14.602 L16.425,14.602 L18.165,10.673 L18.165,14.603 L19.623,14.603 L19.647,9.083 L19.646,9.084 Z M28.986,11.852 L31.517,9.084 L29.695,9.084 L28.094,10.81 L26.546,9.084 L20.652,9.084 L20.652,14.602 L26.462,14.602 L28.076,12.864 L29.624,14.602 L31.499,14.602 L28.987,11.852 L28.986,11.852 Z"></path></g></svg>
                </li>
                &nbsp;&nbsp;
                <li><svg class="payment-icon" viewBox="0 0 38 24" xmlns="http://www.w3.org/2000/svg" role="img" width="38" height="24" aria-labelledby="pi-diners_club"><title id="pi-diners_club">Diners Club</title><path opacity=".07" d="M35 0H3C1.3 0 0 1.3 0 3v18c0 1.7 1.4 3 3 3h32c1.7 0 3-1.3 3-3V3c0-1.7-1.4-3-3-3z"></path><path fill="#fff" d="M35 1c1.1 0 2 .9 2 2v18c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2V3c0-1.1.9-2 2-2h32"></path><path d="M12 12v3.7c0 .3-.2.3-.5.2-1.9-.8-3-3.3-2.3-5.4.4-1.1 1.2-2 2.3-2.4.4-.2.5-.1.5.2V12zm2 0V8.3c0-.3 0-.3.3-.2 2.1.8 3.2 3.3 2.4 5.4-.4 1.1-1.2 2-2.3 2.4-.4.2-.4.1-.4-.2V12zm7.2-7H13c3.8 0 6.8 3.1 6.8 7s-3 7-6.8 7h8.2c3.8 0 6.8-3.1 6.8-7s-3-7-6.8-7z" fill="#3086C8"></path></svg></li>
                &nbsp;&nbsp;
                <li><svg class="payment-icon" viewBox="0 0 38 24" xmlns="http://www.w3.org/2000/svg" role="img" width="38" height="24" aria-labelledby="pi-master"><title id="pi-master">Mastercard</title><path opacity=".07" d="M35 0H3C1.3 0 0 1.3 0 3v18c0 1.7 1.4 3 3 3h32c1.7 0 3-1.3 3-3V3c0-1.7-1.4-3-3-3z"></path><path fill="#fff" d="M35 1c1.1 0 2 .9 2 2v18c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2V3c0-1.1.9-2 2-2h32"></path><circle fill="#EB001B" cx="15" cy="12" r="7"></circle><circle fill="#F79E1B" cx="23" cy="12" r="7"></circle><path fill="#FF5F00" d="M22 12c0-2.4-1.2-4.5-3-5.7-1.8 1.3-3 3.4-3 5.7s1.2 4.5 3 5.7c1.8-1.2 3-3.3 3-5.7z"></path></svg></li>
                &nbsp;&nbsp;
                <li><svg class="payment-icon" viewBox="0 0 38 24" xmlns="http://www.w3.org/2000/svg" role="img" width="38" height="24" aria-labelledby="pi-visa"><title id="pi-visa">Visa</title><path opacity=".07" d="M35 0H3C1.3 0 0 1.3 0 3v18c0 1.7 1.4 3 3 3h32c1.7 0 3-1.3 3-3V3c0-1.7-1.4-3-3-3z"></path><path fill="#fff" d="M35 1c1.1 0 2 .9 2 2v18c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2V3c0-1.1.9-2 2-2h32"></path><path d="M28.3 10.1H28c-.4 1-.7 1.5-1 3h1.9c-.3-1.5-.3-2.2-.6-3zm2.9 5.9h-1.7c-.1 0-.1 0-.2-.1l-.2-.9-.1-.2h-2.4c-.1 0-.2 0-.2.2l-.3.9c0 .1-.1.1-.1.1h-2.1l.2-.5L27 8.7c0-.5.3-.7.8-.7h1.5c.1 0 .2 0 .2.2l1.4 6.5c.1.4.2.7.2 1.1.1.1.1.1.1.2zm-13.4-.3l.4-1.8c.1 0 .2.1.2.1.7.3 1.4.5 2.1.4.2 0 .5-.1.7-.2.5-.2.5-.7.1-1.1-.2-.2-.5-.3-.8-.5-.4-.2-.8-.4-1.1-.7-1.2-1-.8-2.4-.1-3.1.6-.4.9-.8 1.7-.8 1.2 0 2.5 0 3.1.2h.1c-.1.6-.2 1.1-.4 1.7-.5-.2-1-.4-1.5-.4-.3 0-.6 0-.9.1-.2 0-.3.1-.4.2-.2.2-.2.5 0 .7l.5.4c.4.2.8.4 1.1.6.5.3 1 .8 1.1 1.4.2.9-.1 1.7-.9 2.3-.5.4-.7.6-1.4.6-1.4 0-2.5.1-3.4-.2-.1.2-.1.2-.2.1zm-3.5.3c.1-.7.1-.7.2-1 .5-2.2 1-4.5 1.4-6.7.1-.2.1-.3.3-.3H18c-.2 1.2-.4 2.1-.7 3.2-.3 1.5-.6 3-1 4.5 0 .2-.1.2-.3.2M5 8.2c0-.1.2-.2.3-.2h3.4c.5 0 .9.3 1 .8l.9 4.4c0 .1 0 .1.1.2 0-.1.1-.1.1-.1l2.1-5.1c-.1-.1 0-.2.1-.2h2.1c0 .1 0 .1-.1.2l-3.1 7.3c-.1.2-.1.3-.2.4-.1.1-.3 0-.5 0H9.7c-.1 0-.2 0-.2-.2L7.9 9.5c-.2-.2-.5-.5-.9-.6-.6-.3-1.7-.5-1.9-.5L5 8.2z" fill="#142688"></path></svg></li>
        </ul>
    </div>

    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="jquery-migrate-3.0.1.min.js"/>
    <asset:javascript src="bootstrap.min.js"/>
    <asset:javascript src="aos.js"/>
    <asset:javascript src="main.js"/>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.1/html2pdf.bundle.min.js"></script>    
 </body>
</html>


