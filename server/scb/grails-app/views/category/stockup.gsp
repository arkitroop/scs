<!DOCTYPE html>
<html>
    <head>
        <title>Stock update</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1"/>
	    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
	    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" /> -->
	    <!-- <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
        <asset:javascript src="jquery-3.3.1.min.js"/>
        <asset:javascript src="multiselect.min.js"/>
	    <asset:stylesheet src="application.css"/>
   </head>
    <body>
		<nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation">
		    <a class="navbar-brand" href="/#" style="height: 88px;"><asset:image src="favicon.ico" alt="Grails Logo"/></a>
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
		        <span class="navbar-toggler-icon"></span>
		    </button>
	        <div class="admin-heading">Sri Chellas Bakery</div>
    		<div class="collapse navbar-collapse" aria-expanded="false" style="height: 0.8px;" id="navbarContent">
        		<ul class="nav navbar-nav ml-auto">
        		</ul>
        	</div>
			<div class="nav switch-menu" role="navigation">
				<ul>
					<li><g:link class="list" controller="category" action="index">Category</g:link></li>
					<li><g:link class="list" controller="product" action="index">Product</g:link></li>
					<li><g:link class="list" controller="config" action="index">Configs</g:link></li>
					<li><g:link class="create" controller="category" action="stockup">Stock In/Out</g:link></li>
					<li><g:link class="create" controller="image" action="upload">Upload</g:link></li>
					<li><g:link class="delete-new" controller="auth" action="logout">Logout</g:link></li>
				</ul>
			</div>
		</nav>

        <a href="#edit-category" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/dashboard')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" controller="category" action="index">Category List</g:link></li>
            </ul>
        </div>
        <div id="edit-category" class="content scaffold-edit" role="main">
            <g:form type="POST" name="uploadStock" action="updateStock" >
                <fieldset class="form">
                	<div class="fieldcontain required row">
			            <div class="col-sm-5">
			            	<label style="text-align: left;margin-left: 10px;">Stock In <span class="required-indicator">*</span></label>
			                <select name="from" id="optgroup" class="form-control" size="8" multiple="multiple" style="height: 450px;">
	                        <g:each in="${scb.Category.list()}">
                        	  <optgroup label="${it.name}">
                        	  	<g:each in="${it.productList.findAll{it.stockAvailable}}">
		                            <option value="${it.id}">${it.name}</option>
                        	  	</g:each>
                        	  </optgroup>
	                        </g:each>
			                </select>
			            </div>
			            
			            <div class="col-sm-2" style="margin-top: 30px;">
			                <!--
			                <button type="button" id="optgroup_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
			                <button type="button" id="optgroup_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
			                <button type="button" id="optgroup_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
			                <button type="button" id="optgroup_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
			                -->
			                <button type="button" id="optgroup_rightAll" class="btn btn-block" style="background: #ff6b6b;color: white;">Stock Out All</i></button>
			                <button type="button" id="optgroup_rightSelected" class="btn btn-block" style="background: #ffbd6b;color: white;">Stock Out >></i></button>
			                <button type="button" id="optgroup_leftSelected" class="btn btn-block" style="background: #ffbd6b;color: white;"><< Stock In</i></button>
			                <button type="button" id="optgroup_leftAll" class="btn btn-block" style="background: #2cb52a;color: white;">Stock In All</i></button>
			            </div>
			            
			            <div class="col-sm-5">
			            	<label style="text-align: left;margin-left: 10px;">Stock Out <span class="required-indicator">*</span></label>
			                <select name="to" id="optgroup_to" class="form-control" size="8" multiple="multiple" style="height: 450px;">
		                        <g:each in="${scb.Category.list()}">
	                        	  <optgroup label="${it.name}">
	                        	  	<g:each in="${it.productList.findAll{!it.stockAvailable}}">
			                            <option value="${it.id}">${it.name}</option>
	                        	  	</g:each>
	                        	  </optgroup>
		                        </g:each>
			                </select>
			            </div>
                	</div>
                </fieldset>
                <fieldset class="buttons">
                    <input type="button" onClick="submitData()" name="create" class="save" value="Submit" />
                    <span id="success-message"></span>
                </fieldset>
            </g:form>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
			    $('#optgroup').multiselect({multiple:true});
			});
			function submitData(){
		        var productSelected = $('#optgroup option');
		        var productUnSelected = $('#optgroup_to option');
		        var selected = [];
		        var unselected = [];
		        $(productSelected).each(function(index, brand){
		            selected.push($(this).val());
		        });
		        $(productUnSelected).each(function(index, brand){
		            unselected.push($(this).val());
		        });
                jQuery.ajax({
                    url: '/category/stockUpdate',
                    data: {
                    	in: selected,
                    	out: unselected
                    },
                    method: 'POST',
                    success: function(data){
                    	if(data.status){
	                    	$("#success-message").html("<span style='color:green;'>Updated Successfully</span>");
                    	}else{
	                    	$("#success-message").html("<span style='color:red;'>Updation Failure</span>");
                    	}
                    }
				});				
			}
	    </script>
    </body>
</html>
