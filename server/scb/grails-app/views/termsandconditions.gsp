<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <asset:stylesheet src="application.css"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
    <g:set var="configs" value="${scb.Config.list()}" />
    <title>Terms & Conditions</title>
</head>
<body style="padding:100px 200px;">
    <nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation" style="margin-bottom: 50px;border-radius: 5px;">
        <a class="navbar-brand" href="/#"><asset:image src="favicon.ico" alt="Sri Chellas Bakery"/></a>
        <h1 style="color: white;
        font-weight: bold;
        font-size: 30px;margin: 30px;">${configs.find{it.key =='companyName'}.value}</h1>
    </nav>
    
<article><style>
  [data-custom-class='body'], [data-custom-class='body'] * {
          background: transparent !important;
        }
[data-custom-class='title'], [data-custom-class='title'] * {
          font-family: Arial !important;
font-size: 26px !important;
color: #000000 !important;
        }
[data-custom-class='subtitle'], [data-custom-class='subtitle'] * {
          font-family: Arial !important;
color: #595959 !important;
font-size: 14px !important;
        }
[data-custom-class='heading_1'], [data-custom-class='heading_1'] * {
          font-family: Arial !important;
font-size: 19px !important;
color: #000000 !important;
        }
[data-custom-class='heading_2'], [data-custom-class='heading_2'] * {
          font-family: Arial !important;
font-size: 17px !important;
color: #000000 !important;
        }
[data-custom-class='body_text'], [data-custom-class='body_text'] * {
          color: #595959 !important;
font-size: 14px !important;
font-family: Arial !important;
        }
[data-custom-class='link'], [data-custom-class='link'] * {
          color: #3030F1 !important;
font-size: 14px !important;
font-family: Arial !important;
word-break: break-word !important;
        }
</style>
        
      <div data-custom-class="body">
        <div class="main_color container_wrap_first container_wrap fullsize" style=" "><div class="container"><main class="template-page content av-content-full alpha units"><div class="post-entry post-entry-type-page post-entry-688"><div class="entry-content-wrapper clearfix"><section class="av_textblock_section "><div class="avia_textblock "><p><strong>ONLINE ORDER Terms &amp; Conditions</strong></p>

<p><strong>Care for Cakes</strong></p>
<p>After receiving the cake, immediately refrigerate it.</p>
<p>The cake should be consumed within 24 hours.</p>
<p>Every cake or pastry we offer is handcrafted and chef has own way of baking and designing a cake, there might be slight variation in the product in terms of appearance.</p>

<p><strong> <br class="avia-permanent-lb">Delivery Policy </strong></p>
<p><b>srichellasbakery.com</b> accepts online orders to deliver within 5kms range. And only if they notified through whatsap <b>+91 77088 07996</b> reg the delivery details.</p>
<p>if they doesnt mention about delivery in whatsapp then they need to come to shop to pick it up.</p>
<p>we takes full responsibility to send the order carefully.</p>
<p>we uses only self owned bikes and cars for delivery and no 3rd party delivery service is used.</p>
<p>ITS CUSTOMERS SOLE RESPONSIBILITY TO CHECK THE PRODUCT THAN RECEIVE FROM DELIVERY PERSON.</p>
<p>However, once the order is handed over, the temperature maintenance responsibility lies with the receiver.</p>
<p>The order delivery will be done from our production center.</p>
<p>Delivery may not be exactly at the same time.We will attempt delivery of the items once got prepared.</p>
<p>
Estimated delivery time depends on the availability of the product and the destination to which you want the product to be delivered.
It will take minimum of 1hr to maximum 3hr of timeline in delivering the cake to the address mentioned in invoice within 5kms radius.
</p>
<p>You acknowledge that requested delivery time is non-binding.</p>
<p>Delivery may take longer due to Bad weather, Other unforeseen circumstacnes etc…</p>
<p>In case if the recipient is not available, he/she can inform the delivery person to deliver the order to the gate/reception/neighbor.</p>
<p>Proof of delivery will be provided in case of any dispute in delivery.</p>
<p>If there is no one available at the shipping addresses to accept the delivery of your order at the time of delivery, the order will not be considered late.</p>
<p>We shall not be deliver the product other than address mention in invoice.</p>
${configs.find{it.key =='termsAndCondition'}?.value}

<p><strong> <br class="avia-permanent-lb">Duplicate Order</strong></p>
<p>If you have placed two or more identical orders by mistake, please let us know about it on <b>+91 77088 07996</b> We will give a full refund for the duplicate order unless the production is not start for the same.</p>
<p>If we find that the same order has been made twice, we will try to contact the customer to confirm the order. If the customer is not contactable, we will take the decision on your behalf and that will be final.</p>

<p><strong> <br class="avia-permanent-lb">Damage Or Defective Products</strong></p>
<p>If any product received by you is damaged or defective, you can let us know the same. You can call us at <b>+91 77088 07996</b> (10:00 am to 10:00 pm hours, 7 days a week) or send us an e-mail with the photographs of the same at <b>srichellasbakery@gmail.com</b>, within 24 hours of receiving the product.</p>
<p>We will either deliver to you the new product or refund the amount to you after investigation the same.</p>

<p><strong> <br class="avia-permanent-lb">General Terms of Service</strong></p>
<p>This site is copyright protected. All rights reserved to <b>Sri Chellas Bakery and Sweets</b></p>
<p>Any textual or photographic material only for customer view.</p>
<p>All trademarks and logos are related to <b>Sri Chellas Bakery and Sweets</b>. And you may not copy or use them in any manner.</p>
</div></section>
</div></div></main>             
   </div></div>    
    </style>
      </div>
      </div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></article>

      </body>
</html>