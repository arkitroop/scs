<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <asset:stylesheet src="application.css"/>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation">
    <a class="navbar-brand" href="/#"><asset:image src="favicon.ico" alt="Grails Logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
</nav>
<form method="post" action="auth/login" class="login-form">
	<div class="custom-alert-message">${flash.message}</div>
	<div class="form-group" style="width: 400px;margin: auto;">
	    <input type="password" class="form-control" name="password" placeholder="Enter Admin Password" autofocus="true">
	</div>
	<div class="form-group" style="margin: 20px auto;text-align: center;">
	    <button class="btn btn-success">Login</button>
	</div>  
</form>

<asset:javascript src="application.js"/>

</body>
</html>
