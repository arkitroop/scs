<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    

    <asset:stylesheet src="animate.css"/>
    <asset:stylesheet src="owl.carousel.min.css"/>
    <asset:stylesheet src="owl.theme.default.min.css"/>
    <asset:stylesheet src="aos.css"/>
    <asset:stylesheet src="bootstrap-datepicker.css"/>
    <asset:stylesheet src="jquery.timepicker.css"/>
    <asset:stylesheet src="flaticon.css"/>
    <asset:stylesheet src="icomoon.css"/>
    <asset:stylesheet src="style.css"/>
    <asset:javascript src="index.js"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
    
    <g:set var="configs" value="${scb.Config.list()}" />
    <g:set var="categoryList" value="${scb.Category.list().findAll{it.stockAvailable()}}" />
    <g:set var="servepath" value="${grailsApplication.config.getProperty('grails.var.servepath')}"/>
    <g:set var="savepath" value="${grailsApplication.config.getProperty('grails.var.savepath')}"/>
    <title>Sri Chellas Bakery</title>
    <script>var whatsappMsgUrl = "${configs.find{it.key =='whatsappMsgUrl'}.value}";</script>
</head>

<body>   
    <div id="floatingnavbar" class="floatingnavbar">
       <nav> 
        <div>
            <a href="#home" class="navbar-brand">  
                <asset:image src="logo.png" class="chellasnamelogo mobile-floating-logo"/>
            </a>        
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fa fa-bars"></i>
        </label>  
        <!-- <label class="logo">SRI CHELLAS</label>                       -->
          <ul class="">
            <li class="active"><a href="#home" onclick="closeNavBar();">Home</a></li>
            <li class=""><a href="#menu" onclick="closeNavBar();">Menu</a></li>
            <li class=""><a href="#about" onclick="closeNavBar();">About</a></li>
            <li class=""><a href="#contact" onclick="closeNavBar();">Contact Us</a></li>
         </ul>                      
        </div>
    </nav>
    </div>
    <div id="home">
        <nav>
            <div>
                <a href="#home" class="navbar-brand">  
                    <asset:image src="logo.png" class="chellasnamelogo mobile-logo"/>     
                </a>            
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn">
                <i class="fa fa-bars"></i>
            </label>  
            <!-- <label class="logo">SRI CHELLAS</label>                       -->
              <ul class="">
                <li class="active"><a href="#home" onclick="closeNavBar();">Home</a></li>
                <li class=""><a href="#menu" onclick="closeNavBar();">Menu</a></li>
                <li class=""><a href="#about" onclick="closeNavBar();">About</a></li>
                <li class=""><a href="#contact" onclick="closeNavBar();">Contact Us</a></li>
            </ul>                      
            </div>
        </nav>
    </div>
<!--    <section class="topbarsection"></section>     -->

    <!-- slider  section start -->

     <section class="">
        <asset:image src="temp.jpeg" class="chellasBgPicture"/>
        <ul class="topicon" style="color : #ffffff">
            <li>            
                <figure>
                  <!-- <span class="sprite-svg birthday1"></span> -->
                  <asset:image src="birthdaycakes.jpeg" class="sprite-svg"/>
                  <figcaption class="colorBlack">Birthday</figcaption>
                </figure>               
            </li>            
            <li>             
                <figure>
                  <!-- <span class="sprite-svg birthday1"></span> -->
                  <asset:image src="party.png" class="sprite-svg"/>
                  <figcaption class="colorBlack">Party</figcaption>
                </figure>
              </a>
            </li>
            <li>
                <figure>
                 <!--  <span class="sprite-svg birthday1"></span> -->
                  <asset:image src="pinata.jpeg" class="sprite-svg"/> 
                  <figcaption class="colorBlack">Pinata</figcaption>
                </figure>
            </li>
            <li>
                <figure>                    
                  <asset:image src="newyearcake.jpeg" class="sprite-svg"/>
                  <!-- <span class="sprite-svg birthday1"></span> -->
                  <figcaption class="colorBlack">New Year</figcaption>
                </figure>
            </li>
            <li>
                <figure>
                  <!-- <span class="sprite-svg birthday1"></span> -->
                  <asset:image src="christmascake.png" class="sprite-svg"/>
                  <figcaption class="colorBlack">Christmas</figcaption>
                </figure>
            </li>
            <li>
                <figure>
                  <!-- <span class="sprite-svg birthday1"></span> -->                  
                  <asset:image src="anniversarycake.jpeg" class="sprite-svg"/>
                  <figcaption class="colorBlack">Anniversary</figcaption>
                </figure>
            </li>
            <li>
                <figure>
                  <!-- <span class="sprite-svg birthday1"></span> -->
                  <asset:image src="valentinecake.png" class="sprite-svg"/>
                  <figcaption class="colorBlack">Valentine</figcaption>
                </figure>
            </li>
            <li>              
                <figure>
                  <asset:image src="bombcake.jpeg" class="sprite-svg"/>
                  <!-- <span class="sprite-svg birthday1"></span> -->
                  <figcaption class="colorBlack">Bomb Cake</figcaption>
                </figure>              
            </li>
        </ul>      
    </section>

     <!--section class=" home-slider owl-carousel js-fullheight">
        <div class="slider-item js-fullheight" style="background-image: url(assets/scb-shop.jpeg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text js-fullheight justify-content-center align-items-center">
                    <div class="col-md-12 col-sm-12 text-center ftco-animate">
                        <span class="subheading">${configs.find{it.key =='companyName'}.value}</span> 
                    </div>
                </div>
            </div>
        </div>
    </section-->
    <!-- slider  section end  -->

    
<!-- cake section starts -->
    <section class="ftco-section" id="menu">
        <div class="categoryOverlay"></div>
        <div>
            <div class="col-12 text-center heading-section ftco-animate">
                <span class="subheading subheading-special">Specialities</span>
            </div>
        </div>
        <div class="search">
            <input id="SearchTerm" type="search" class="searchTerm" placeholder="Search by category"  onkeyDown="return searchByCategory(event)" onkeypress="return searchByCategory(event)">
            <button type="submit" class="searchButton" style="outline:none;">
                 <i class="fa fa-search"></i>
             </button>
       </div>
        <div id="categoryList" class="container-fluid px-4">
            <g:each in="${categoryList}">
                <g:set var="products" value="${scb.Product.findAllByCategoryAndStockAvailable(it,true)}"/>

                <g:set var="imgList" value="${scb.Image.findAllByEntityAndEntityIdInList(scb.ImageEntity.PRODUCT,products.id).reverse()}" />
                <g:set var="imgs" value="${imgList.findAll{new File(savepath+it.fileName).exists() == true}.take(4)}" />

                <g:if test="${imgs != null && imgs.size() > 0}">
                     <a href="/category/showProducts/${it.id}" class="categoryIter" style="color: inherit;text-decoration:none;">                       
                        <div class="menus d-flex col-12 p-0 mobile-menu">
                            <div class="img-club-${imgs.size()}">
                                <g:each var="img" in="${imgs}">
                                    <asset:image class="menu-img img" src="${servepath+img.fileName}"/>
                                </g:each>
                            </div>
                            <div class="text">
                                <div class="d-flex">
                                    <div class="">
                                        <h6 class="category-title" style="display: flex;"> 
                                            <span style="word-break:break-word;">${it.name}</span>
                                            <span class="count-badge">${products.size()}</span>
                                        </h6>
                                        <h6 class="descFontWeight">${it.description}</h6>
                                    </div>                                    
                                </div>                                       
                        </div>                            
                    </div>
                    </a>
                </g:if>
            </g:each>            
                <div id="noResultsDiv" class="dN">                    
                    <asset:image src="notfound.png" class="notfound"/>
                    <span >No results found</span>
                </div>
        </div>
    </section>
    <!-- cake section ends -->

    <!-- about us section starts  -->
    <section class="ftco-section testimony-section img" id="about" style="background: #0000009e;padding-top: 160px;">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                    <span class="subheading">About us</span>
                </div>
            </div>
            <div class="column">
                <div class="about-main-content-wrapper">
                    <asset:image src="scb-shop.jpeg" class="aboutImg"/>
                    <div class="about-main-content">
                        ${configs.find{it.key =='aboutMainContent'}?.value}
                    </div>
                </div>
                <div class="item mobile-aboutus" style="display: flex;gap: 40px;margin-top: 80px;">
                    <div class="testimony-wrap py-4">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-quote-left"></span>
                        </div>
                        <div class="text">
                            <p class="mb-4">
                                <p>${configs.find{it.key =='aboutStory1'}.value}</p>
                            </p>
                        </div>
                    </div>
                    <div class="testimony-wrap py-4">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-quote-left"></span>
                        </div>
                        <div class="text">
                            <p class="mb-4">
                                <p>${configs.find{it.key =='aboutStory2'}.value}</p>
                            </p>
                        </div>
                    </div>
                    <div class="testimony-wrap py-4">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-quote-left"></span>
                        </div>
                        <div class="text">
                            <p class="mb-4">
                                <p>${configs.find{it.key =='aboutStory3'}.value}</p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-wrap-about">        
        <div class="intro">
            <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6 intro_col">
                    <div class="intro_image"><asset:image src="b1.jpg" alt="intro"/></div>
                </div>
                <div class="col-xl-4 col-md-6 intro_col">
                    <div class="intro_image"><asset:image src="b2.jpg" alt="intro"/></div>
                </div>
                <div class="col-xl-4 col-md-6 intro_col">
                    <div class="intro_image"><asset:image src="b3.jpg" alt="intro"/></div>
                </div>
            </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-counter img ftco-no-pt" id="section-counter">
        <div class="container">
            <div class="row d-md-flex">
                <div class="col-md-9">
                    <div class="row d-md-flex align-items-center">
                        <div
                            class="col-md-6 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="${configs.find{it.key =='experienceValue'}.value}">0</strong>
                                    <span>Years of Experience</span>
                                </div>
                            </div>
                        </div>
                        <div
                            class="col-md-6 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="${configs.find{it.key== 'cakeValue'}.value}">0</strong>
                                    <span>Cakes/Variety</span>
                                </div>
                            </div>
                        </div>
                        <div
                            class="col-md-6 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="${configs.find{it.key== 'staffsValue'}.value}">0</strong>
                                    <span>Staffs</span>
                                </div>
                            </div>
                        </div>
                        <div
                            class="col-md-6 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="${configs.find{it.key== 'customerValue'}.value}">0</strong>
                                    <span>Happy Customers</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center text-md-left mobile-websitestory">
                    <p>${configs.find{it.key =='websiteStoryIntro'}.value}</p>
                </div>
            </div>
        </div>
    </section>

    <!-- about us section ends  -->

    <!-- footer section starts -->
    <section class="ftco-footer ftco-bg-dark ftco-section">
        <div class="container" id="contact">
            <div class="row mb-5">
                <div class="col-md-6 col-lg-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">${configs.find{it.key =='companyName'}.value}</h2>
                        <p class="ftco-info">
                            ${configs.find{it.key =='websiteStoryFooter'}.value}
                            <br><br>
                            <span class="fa fa-map-marker" style="font-size:18px"></span>
                            &nbsp; ${configs.find{it.key =='companyAddress'}.value}
                            <br><br>
                            <span class="fa fa-phone" style="font-size:18px"></span>
                            &nbsp;${configs.find{it.key =='adminContactNumber'}.value}
                            <br><br>                                 
                            <span class="fa fa-envelope" style="font-size:18px"></span>
                            &nbsp;${configs.find{it.key =='adminEmailAddress'}.value}    
                        </p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                            <li class=""><a href="${configs.find{it.key =='facebookUrl'}.value}" target="_blank"><span class="fa fa-facebook"></span></a></li>
                            <li class=""><a href="${configs.find{it.key =='whatsappMsgUrl'}.value}" target="_blank"><span class="fa fa-whatsapp"></span></a></li>
                            <li class=""><a href="${configs.find{it.key =='instagramUrl'}.value}" target="_blank"><span class="fa fa-instagram"></span></a></li>
                             <li class=""><a href="https://goo.gl/maps/dVmT9reKmEdYdeYM7" target="_blank"><span class="fa fa-map-marker"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Open Hours</h2>
                        <ul class="list-unstyled open-hours">
                            <li class="d-flex"><span>Monday</span><span>6:00am - 10:00pm</span></li>
                            <li class="d-flex"><span>Tuesday</span><span>6:00am - 10:00pm</span></li>
                            <li class="d-flex"><span>Wednesday</span><span>6:00am - 10:00pm</span></li>
                            <li class="d-flex"><span>Thursday</span><span>6:00am - 10:00pm</span></li>
                            <li class="d-flex"><span>Friday</span><span>6:00am - 10:00pm</span></li>
                            <li class="d-flex"><span>Saturday</span><span>6:00am - 10:00pm</span></li>
                            <li class="d-flex"><span>Sunday</span><span> 6:00am - 10:00pm</span></li>
                        </ul>

                        
                    </div>

                    <!-- <div class="ftco-footer-widget mb-4"> -->
                        <h2 class="ftco-heading-2">Policies</h2>
                        <br/>
                        <span class="fa fa-hand-o-right" style="font-size:15px"></span>
                        <a style="color:#0056b3;" href="/termsandconditions" target="_blank">Terms and Conditions</a>
                        <br/><br/>
                        <span class="fa fa-hand-o-right" style="font-size:15px"></span>
                        <a style="color:#0056b3;" href="/refundandcancellations" target="_blank">Refund and Cancellations</a>
                        <br/><br/>
                        <span class="fa fa-hand-o-right" style="font-size:15px"></span>
                        <a style="color:#0056b3;" href="/policy" target="_blank">Our Privacy Policy</a>
                    <!-- </div> -->
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Instagram</h2>
                            <div class="thumb insta-img">
                                <a href="https://www.instagram.com/p/CT1BBtcBdaf/" target="_blank" class="thumb-menu img" style="background-image: url(assets/insta_image_1.jpg);"></a>
                                <a href="https://www.instagram.com/p/CGH4icmFbXk/" target="_blank" class="thumb-menu img" style="background-image: url(assets/insta_image_2.jpg);"></a>
                            </div>
                            <div class="thumb insta-img">
                                <a href="https://www.instagram.com/p/CFyULbnFTQ5/" target="_blank" class="thumb-menu img" style="background-image: url(assets/insta_image_3.jpg);"></a>
                                <a href="https://www.instagram.com/p/CKTPy0ml7hu/" target="_blank" class="thumb-menu img" style="background-image: url(assets/insta_image_4.jpg);"></a>
                            </div>
                            <div class="thumb insta-img">
                                <a href="https://www.instagram.com/p/CIZx7-0FjMg/" target="_blank" class="thumb-menu img" style="background-image: url(assets/insta_image_5.jpg);"></a>
                                <a href="https://www.instagram.com/p/CG4Wbajlj44/" target="_blank" class="thumb-menu img" style="background-image: url(assets/insta_image_6.jpg);"></a>
                            </div>  
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Chat with us!</h2>
                        <p>We are here to serve you better, Raise your query.</p>
                        <form action="#" class="subscribe-form">
                            <div class="form-group">
                                <input id="chatText" type="text" required class="form-control mb-2">
                                <!-- <input type="submit" onclick= value="Send" class="form-control submit px-3"> -->
                                <button class="form-control submit px-3" onclick="chatWithUs()"><span>Send</span></button>      
                            </div>
                        </form>
                        <br><br>
                        <h2 class="ftco-heading-2">Other Branch Address</h2>
                        <span class="fa fa-map-marker" style="font-size:18px"></span>
                            &nbsp; ${configs.find{it.key =='branchAddress1'}.value}
                        
                    </div>
                </div>                
            </div>
        </div>
        <div class="footer-copyright">
            <span>© 2022, ${configs.find{it.key =='companyName'}.value}. All Rights Reserved.</span>            
        </div>
        <ul class="payment-icon-list">          
                <li><svg class="payment-icon" xmlns="http://www.w3.org/2000/svg" role="img" viewBox="0 0 38 24" width="38" height="24" aria-labelledby="pi-american_express"><title id="pi-american_express">American Express</title><g fill="none"><path fill="#000" d="M35,0 L3,0 C1.3,0 0,1.3 0,3 L0,21 C0,22.7 1.4,24 3,24 L35,24 C36.7,24 38,22.7 38,21 L38,3 C38,1.3 36.6,0 35,0 Z" opacity=".07"></path><path fill="#006FCF" d="M35,1 C36.1,1 37,1.9 37,3 L37,21 C37,22.1 36.1,23 35,23 L3,23 C1.9,23 1,22.1 1,21 L1,3 C1,1.9 1.9,1 3,1 L35,1"></path><path fill="#FFF" d="M8.971,10.268 L9.745,12.144 L8.203,12.144 L8.971,10.268 Z M25.046,10.346 L22.069,10.346 L22.069,11.173 L24.998,11.173 L24.998,12.412 L22.075,12.412 L22.075,13.334 L25.052,13.334 L25.052,14.073 L27.129,11.828 L25.052,9.488 L25.046,10.346 L25.046,10.346 Z M10.983,8.006 L14.978,8.006 L15.865,9.941 L16.687,8 L27.057,8 L28.135,9.19 L29.25,8 L34.013,8 L30.494,11.852 L33.977,15.68 L29.143,15.68 L28.065,14.49 L26.94,15.68 L10.03,15.68 L9.536,14.49 L8.406,14.49 L7.911,15.68 L4,15.68 L7.286,8 L10.716,8 L10.983,8.006 Z M19.646,9.084 L17.407,9.084 L15.907,12.62 L14.282,9.084 L12.06,9.084 L12.06,13.894 L10,9.084 L8.007,9.084 L5.625,14.596 L7.18,14.596 L7.674,13.406 L10.27,13.406 L10.764,14.596 L13.484,14.596 L13.484,10.661 L15.235,14.602 L16.425,14.602 L18.165,10.673 L18.165,14.603 L19.623,14.603 L19.647,9.083 L19.646,9.084 Z M28.986,11.852 L31.517,9.084 L29.695,9.084 L28.094,10.81 L26.546,9.084 L20.652,9.084 L20.652,14.602 L26.462,14.602 L28.076,12.864 L29.624,14.602 L31.499,14.602 L28.987,11.852 L28.986,11.852 Z"></path></g></svg>
                </li>
                &nbsp;&nbsp;
                <li><svg class="payment-icon" viewBox="0 0 38 24" xmlns="http://www.w3.org/2000/svg" role="img" width="38" height="24" aria-labelledby="pi-diners_club"><title id="pi-diners_club">Diners Club</title><path opacity=".07" d="M35 0H3C1.3 0 0 1.3 0 3v18c0 1.7 1.4 3 3 3h32c1.7 0 3-1.3 3-3V3c0-1.7-1.4-3-3-3z"></path><path fill="#fff" d="M35 1c1.1 0 2 .9 2 2v18c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2V3c0-1.1.9-2 2-2h32"></path><path d="M12 12v3.7c0 .3-.2.3-.5.2-1.9-.8-3-3.3-2.3-5.4.4-1.1 1.2-2 2.3-2.4.4-.2.5-.1.5.2V12zm2 0V8.3c0-.3 0-.3.3-.2 2.1.8 3.2 3.3 2.4 5.4-.4 1.1-1.2 2-2.3 2.4-.4.2-.4.1-.4-.2V12zm7.2-7H13c3.8 0 6.8 3.1 6.8 7s-3 7-6.8 7h8.2c3.8 0 6.8-3.1 6.8-7s-3-7-6.8-7z" fill="#3086C8"></path></svg></li>
                &nbsp;&nbsp;
                <li><svg class="payment-icon" viewBox="0 0 38 24" xmlns="http://www.w3.org/2000/svg" role="img" width="38" height="24" aria-labelledby="pi-master"><title id="pi-master">Mastercard</title><path opacity=".07" d="M35 0H3C1.3 0 0 1.3 0 3v18c0 1.7 1.4 3 3 3h32c1.7 0 3-1.3 3-3V3c0-1.7-1.4-3-3-3z"></path><path fill="#fff" d="M35 1c1.1 0 2 .9 2 2v18c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2V3c0-1.1.9-2 2-2h32"></path><circle fill="#EB001B" cx="15" cy="12" r="7"></circle><circle fill="#F79E1B" cx="23" cy="12" r="7"></circle><path fill="#FF5F00" d="M22 12c0-2.4-1.2-4.5-3-5.7-1.8 1.3-3 3.4-3 5.7s1.2 4.5 3 5.7c1.8-1.2 3-3.3 3-5.7z"></path></svg></li>
                &nbsp;&nbsp;
                <li><svg class="payment-icon" viewBox="0 0 38 24" xmlns="http://www.w3.org/2000/svg" role="img" width="38" height="24" aria-labelledby="pi-visa"><title id="pi-visa">Visa</title><path opacity=".07" d="M35 0H3C1.3 0 0 1.3 0 3v18c0 1.7 1.4 3 3 3h32c1.7 0 3-1.3 3-3V3c0-1.7-1.4-3-3-3z"></path><path fill="#fff" d="M35 1c1.1 0 2 .9 2 2v18c0 1.1-.9 2-2 2H3c-1.1 0-2-.9-2-2V3c0-1.1.9-2 2-2h32"></path><path d="M28.3 10.1H28c-.4 1-.7 1.5-1 3h1.9c-.3-1.5-.3-2.2-.6-3zm2.9 5.9h-1.7c-.1 0-.1 0-.2-.1l-.2-.9-.1-.2h-2.4c-.1 0-.2 0-.2.2l-.3.9c0 .1-.1.1-.1.1h-2.1l.2-.5L27 8.7c0-.5.3-.7.8-.7h1.5c.1 0 .2 0 .2.2l1.4 6.5c.1.4.2.7.2 1.1.1.1.1.1.1.2zm-13.4-.3l.4-1.8c.1 0 .2.1.2.1.7.3 1.4.5 2.1.4.2 0 .5-.1.7-.2.5-.2.5-.7.1-1.1-.2-.2-.5-.3-.8-.5-.4-.2-.8-.4-1.1-.7-1.2-1-.8-2.4-.1-3.1.6-.4.9-.8 1.7-.8 1.2 0 2.5 0 3.1.2h.1c-.1.6-.2 1.1-.4 1.7-.5-.2-1-.4-1.5-.4-.3 0-.6 0-.9.1-.2 0-.3.1-.4.2-.2.2-.2.5 0 .7l.5.4c.4.2.8.4 1.1.6.5.3 1 .8 1.1 1.4.2.9-.1 1.7-.9 2.3-.5.4-.7.6-1.4.6-1.4 0-2.5.1-3.4-.2-.1.2-.1.2-.2.1zm-3.5.3c.1-.7.1-.7.2-1 .5-2.2 1-4.5 1.4-6.7.1-.2.1-.3.3-.3H18c-.2 1.2-.4 2.1-.7 3.2-.3 1.5-.6 3-1 4.5 0 .2-.1.2-.3.2M5 8.2c0-.1.2-.2.3-.2h3.4c.5 0 .9.3 1 .8l.9 4.4c0 .1 0 .1.1.2 0-.1.1-.1.1-.1l2.1-5.1c-.1-.1 0-.2.1-.2h2.1c0 .1 0 .1-.1.2l-3.1 7.3c-.1.2-.1.3-.2.4-.1.1-.3 0-.5 0H9.7c-.1 0-.2 0-.2-.2L7.9 9.5c-.2-.2-.5-.5-.9-.6-.6-.3-1.7-.5-1.9-.5L5 8.2z" fill="#142688"></path></svg></li>
        </ul>
    </section>
    <!-- footer section ends -->

    <!-- scripts -->  
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="jquery-migrate-3.0.1.min.js"/>
    <asset:javascript src="bootstrap.min.js"/>
    <asset:javascript src="jquery.waypoints.min.js"/>
    <asset:javascript src="jquery.stellar.min.js"/>
    <asset:javascript src="owl.carousel.min.js"/>
    <asset:javascript src="jquery.magnific-popup.min.js"/>
    <asset:javascript src="aos.js"/>
    <asset:javascript src="jquery.animateNumber.min.js"/>
    <asset:javascript src="bootstrap-datepicker.js"/>
    <asset:javascript src="jquery.timepicker.min.js"/>
    <asset:javascript src="scrollax.min.js"/> 
    <asset:javascript src="main.js"/>


</body>
</html>
