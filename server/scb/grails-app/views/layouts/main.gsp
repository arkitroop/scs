<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <asset:stylesheet src="application.css"/>
        <style>#list-category table tbody tr td:nth-child(3) ul{
    max-height: 60px;
    overflow: auto;}</style>

    <g:layoutHead/>
    <asset:javascript src="jquery.min.js"/>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation">
    <a class="navbar-brand" href="/#"><asset:image src="favicon.ico" alt="Grails Logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="admin-heading">Sri Chellas Bakery</div>

    <div class="collapse navbar-collapse" aria-expanded="false" style="height: 0.8px;" id="navbarContent">
        <ul class="nav navbar-nav ml-auto">
            <g:pageProperty name="page.nav"/>
        </ul>
    </div>
    <div class="nav switch-menu" role="navigation">
        <ul>
            <li><g:link class="list" controller="order" action="index">Order</g:link></li>
            <li><g:link class="list" controller="category" action="index">Category</g:link></li>
            <li><g:link class="list" controller="product" action="index">Product</g:link></li>
            <li><g:link class="list" controller="config" action="index">Configs</g:link></li>
            <li><g:link class="create" controller="category" action="stockup">Stock In/Out</g:link></li>
            <li><g:link class="create" controller="image" action="upload">Upload</g:link></li>
            <li><g:link class="delete-new" controller="auth" action="logout">Logout</g:link></li>
        </ul>
    </div>
</nav>


<g:layoutBody/>


<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
