<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Dashboard</title>
    </head>
    <body>
        <a href="#create-category" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/dashboard')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
		<div class="box-group">
			<div class="box" onclick="window.location.href='/category'">
				<label>Category</label>
				<span>${scb.Category.count()}</span>
			</div>
			<div class="box" onclick="window.location.href='/product'">
				<label>Product</label>
				<span>${scb.Product.countByStockAvailable(true)}<span style="font-size: 16px;">/${scb.Product.count()}</span></span>
			</div>
			<div class="box" onclick="window.location.href='/config'">
				<label>Configs</label>
				<span>${scb.Config.count()}</span>
			</div>
			<!-- <div>${scb.Config.findByKey("websiteHit")?.value}</div> -->
		</div>
    </body>
</html>
