<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <g:set var="servepath" value="${grailsApplication.config.getProperty('grails.var.servepath')}"/>
        <g:set var="savepath" value="${grailsApplication.config.getProperty('grails.var.savepath')}"/>
        <style>.img-list{display: flex;flex-direction: row;margin-left: 25%;gap: 20px;margin-bottom: 20px;overflow: auto;height: 160px;}</style>
    </head>
    <body>
        <a href="#show-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/dashboard')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-product" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="product" />
            <div class="img-list">
                <g:each in="${scb.Image.findAllByEntityAndEntityId(scb.ImageEntity.PRODUCT, this.product.id).reverse()}">
                    <asset:image width="200" height="150" src="${servepath+it.fileName}" alt="" />
                </g:each>            
            </div>
            <g:form resource="${this.product}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.product}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
