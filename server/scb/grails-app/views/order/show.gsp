<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-order" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/dashboard')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-order" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="order" />
            <fieldset class="buttons" style="display: none;">
                <g:if test="${this.order.status == 0}">
                    <g:link class="save btn" onclick="doAction(event,1);">Accept</g:link>
                    <g:link class="delete" onclick="doAction(event,3)">Reject</g:link>
                </g:if>
                <g:if test="${this.order.status == 1}">
                    <g:link class="edit" onclick="doAction(event,2)">Delivered</g:link>
                    <g:link class="delete" onclick="doAction(event,3)">Reject</g:link>
                </g:if>
                <g:if test="${this.order.status == 2}">
                    <g:link class="delete" onclick="doAction(event,0)">Reset</g:link>
                </g:if>
                <g:if test="${this.order.status == 3}">
                    <g:link class="save" onclick="doAction(event,1)">Re-Accept</g:link>
                </g:if>
                <div style="float:right;display: none;">
                    <button class="btn btn-danger" style="margin-right: 20px;" onclick="sendWhatsapp(true)"><span class="fa fa-whatsapp"></span>Send Whatsapp to Customer</button>                   
                    <button class="btn btn-success" style="margin-right: 20px;" onclick="sendWhatsapp(false)"><span class="fa fa-whatsapp"></span>Send Whatsapp to Team</button>
                </div>
            </fieldset>
        </div>
        <script type="text/javascript">
            function sendWhatsapp(isCustomer){
                var whatsappMsgUrl = "${scb.Config.findByKey('whatsappMsgUrl')?.value}";
                var whatsappTeamUrl = "https://wa.me";
                var customerMsgUrl = "https://wa.me/+91${this.order.mobileNumber}";
                var msgUrl = isCustomer ? customerMsgUrl : whatsappTeamUrl;
                var imgLink = window.location.origin+"/assets/${imgpath}";
                var content;
                if(isCustomer){
                    content =  "Dear " + "${this.order.name}" + "," + "%0a" +  
                    "   Your order has been received by us. We are in the process of making and it will be delivered on time." + "%0a" + "%0a" +  
                    "Please make a payment to our UPI ID - *amzn0007708055@apl*" + "%0a" +
                    "And leave a screenshot of the payment here!"; 
                }else{
                    content=encodeURIComponent("http://65.0.97.129/assets/dyup/y9aNghFTVA.jpg")
                    // content = 
                    //   "Product : " +  "${this.order.product?.name} : " +imgLink + "%0a" +
                    //   "Name : " + "${this.order.name}" + "%0a" +
                    //   "Address : " + "${this.order.address}" + "%0a" +  
                    //   "Phone : " + "${this.order.mobileNumber}" + "%0a" +
                    //   "Date : " + "${this.order.date}" + "%0a" +  
                    //   "Time : " + "${this.order.time}" + "%0a" +
                    //   "Kg : "  + "${this.order.weight}" + "%0a" +
                    //   "Price : "  + "₹ ${this.order.price}" + "%0a" +
                    //   "Message : "  + "${this.order.message}";
                }                
                window.open(msgUrl+`?text=`+content, '_blank');
            }

            function doAction(e,status){
                e.preventDefault();
                jQuery.ajax({
                    url: '/order/updateStatus',
                    data: {id:'${this.order.id}', status: status},
                    method: 'POST',
                    success: function(data){
                        window.location.reload();
                    }
                });
            }
        </script>
    </body>
</html>
