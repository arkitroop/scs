<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <style type="text/css">
            .filter-order{
                margin-left: 120px;
                margin-top: -28px;
                margin-bottom: 20px;
                display: flex;
                align-items: center;
            }
            input[name="orderedDate"],input[name="deliveryDate"]{
                padding: 0px;
                width: 180px;
                text-indent: 10px;
                height: 26px;
            }
            .sortable{
                pointer-events: none;
            }
            .cancelbtn:hover, .cancelbtn:active, .cancelbtn:focus, .cancelbtn:hover:active, .cancelbtn:active:focus{
                outline: none;
            }
            .cancelbtn{
                height: 24px;
                outline: none;
                border: 1px solid #cccccc;
                background: white;
                color: grey;
                margin: 0px 5px;
                display: flex;
                align-items: center;
                font-size: 12px;
                cursor: pointer;
            }
        </style>
        <asset:stylesheet src="daterangepicker.css"/>
    </head>
    <body>
        <a href="#list-order" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/dashboard')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
        <div id="list-order" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <div class="filter-order">
                <!-- <h6 style="margin:0px 10px;">Status</h6>
                <select id="order-status" onChange="changeOrderList()">
                    <option value="0">New Orders</option>
                    <option value="1">Accepted</option>
                    <option value="2">Delivered</option>
                    <option value="3">Rejected</option>
                </select>
                <h6 style="margin:0px 10px;">Date</h6>
                <select id="order-date" onChange="changeOrderList()">
                    <option value="0">All</option>
                    <option value="1">Today</option>
                    <option value="3">Last 3 days</option>
                    <option value="7">Last 7 days</option>
                    <option value="30">Last 30 days</option>
                </select> -->
                <h6 style="margin:0px 10px;">Ordered Date</h6>
                <input type="text" name="orderedDate" value="" autocomplete="off"/>
                <button class="cancelbtn" onclick="clearDates('orderedDate')">X</button>
                <h6 style="margin:0px 10px;">Delivery Date</h6>
                <input type="text" name="deliveryDate" value="" autocomplete="off"/>
                <button class="cancelbtn" onclick="clearDates('deliveryDate')">X</button>
            </div>
            <f:table collection="${orderList}"
             properties="scbId, name, mobileNumber, message, weights, orderedDate, deliveryDate, telegramNotification, paidStatus"/>

            <div class="pagination">
                <g:paginate params="${params}" total="${orderCount ?: 0}" />
            </div>
        </div>
        <asset:javascript src="jquery.min.js"/>
        <asset:javascript src="moment.min.js"/>
        <asset:javascript src="daterangepicker.min.js"/>
        <script type="text/javascript">
            let orderedDate='${orderedDate}';
            let deliveryDate='${deliveryDate}';
            $('input[name="orderedDate"]').val(orderedDate);
            $('input[name="deliveryDate"]').val(deliveryDate);
            let defaultDateFormat="DD/MM/YYYY";
            (function() {
                var tempStartDate = moment().subtract(4, 'days'), tempEndDate = moment();
                const ranges = {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                };
                // $("#order-status").val('${status}');
                // $("#order-date").val('${date}');
                $('input[name="orderedDate"]').daterangepicker({
                    autoUpdateInput: false,
                    startDate: orderedDate ? orderedDate.split(" - ")[0]: tempStartDate,
                    endDate: orderedDate ? orderedDate.split(" - ")[1]: tempEndDate,
                    ranges: ranges,
                    locale: {format: defaultDateFormat},
                    maxDate: moment()
                }, function(start, end, label) {
                    orderedDate=start.format(defaultDateFormat)+' - '+end.format(defaultDateFormat);
                    changeOrderList();
                });
                $('input[name="deliveryDate"]').daterangepicker({
                    autoUpdateInput: false,
                    startDate: deliveryDate ? deliveryDate.split(" - ")[0]: tempStartDate,
                    endDate: deliveryDate ? deliveryDate.split(" - ")[1]: tempEndDate,
                    ranges: ranges,
                    locale: {format: defaultDateFormat}
                }, function(start, end, label) {
                    deliveryDate=start.format(defaultDateFormat)+' - '+end.format(defaultDateFormat);
                    changeOrderList();
                });
            })();
            function changeOrderList(){
                window.location.href="/order/index?orderedDate="+orderedDate+"&deliveryDate="+deliveryDate;
            }
            function clearDates(field){
                if(field === "orderedDate"){
                    window.location.href="/order/index?orderedDate=&deliveryDate="+deliveryDate;
                }
                if(field === "deliveryDate"){
                    window.location.href="/order/index?orderedDate="+orderedDate+"&deliveryDate=";
                }
            }
        </script>
    </body>
</html>