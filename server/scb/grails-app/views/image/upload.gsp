<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
        <title>Upload Image</title>
    </head>
    <body>
        <a href="#show-category" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/dashboard')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
        <div id="create-product" class="content scaffold-create" role="main">
            <h1>
                Upload Image
            </h1>
            <div id="image-upload-status" class="message dispnone" role="status"></div>

            <g:uploadForm type="POST" name="uploadImage" action="uploadImage" >
                <fieldset class="form">
                    <div class="fieldcontain required">
                        <label for="name">Select ( Category / Product )
                          <span class="required-indicator">*</span>
                        </label>
                        <select id="entity" name="entity" onChange="updateSelect()">
                            <option value="product" selected>Product</option>
                            <option value="category">Category</option>
                        </select>
                    </div>
                    <div class="fieldcontain required" id="productIdDiv">
                        <label for="name">Select Product
                          <span class="required-indicator">*</span>
                        </label>
                        <select id="productId" name="productId">
                            <g:each in="${productList}">
                                <option value="${it.id}">${it.name}</option>
                            </g:each>
                        </select>
                    </div>
                    <div class="fieldcontain required dispnone" id="categoryIdDiv">
                        <label for="name">Select Category
                          <span class="required-indicator">*</span>
                        </label>
                        <select id="categoryId" name="categoryId" class="">
                            <g:each in="${categoryList}">
                                <option value="${it.id}">${it.name}</option>
                            </g:each>
                        </select>
                    </div>
                    <div class="fieldcontain required">
                        <label for="name">Select Image file
                          <span class="required-indicator">*</span>
                        </label>
                        <div class="file-input"> 
                            <label onclick="clickFile()">Select Image</label>
                            <input type="file" name="file" id="file" class="file" accept="image/png, image/gif, image/jpeg, image/jpg, image/svg"/
                            onchange="onFileSelect()">
                        </div>
                    </div>
                    <div class="fieldcontain required">
                        <label for="name">
                        </label>
                        <span id="selected-file"></span>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <input type="button" onClick="submitData()" name="create" class="save" value="Submit" />
                </fieldset>
        </g:uploadForm>
            <script>
                function clickFile(){
                    $("#file").trigger("click");
                }
                function onFileSelect(){
                    $("#selected-file").html("");
                    jQuery.each(jQuery('#file')[0].files, function(i, file) {
                        var size = Math.ceil(file.size/1024);
                        size = 1024 > size ? size+"kb" : Math.round(size/1024)+"mb";
                        $("#selected-file").html("File: ("+file.name+"), type: ("+file.type+")" +", size: ("+size+")");
                    });
                }
                function submitData(){
                    $("#image-upload-status").addClass("dispnone");
                    var data = new FormData();
                    data.append("entity", $("#entity").val());
                    data.append("productId", $("#productId").val());
                    data.append("categoryId", $("#categoryId").val());
                    jQuery.each(jQuery('#file')[0].files, function(i, file) {
                        data.append('file', file);
                    });
                    jQuery.ajax({
                        url: '/image/uploadImage',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function(data){
                            if(data.status){
                                $("#image-upload-status").removeClass("dispnone");
                                $("#image-upload-status").html("Image upload Success");
                            }else{
                                $("#image-upload-status").removeClass("dispnone");
                                $("#image-upload-status").html("Image upload Failed, Reason : "+data.message);
                            }
                        }
                    });                
                }
                function updateSelect(){
                    var value = $("#entity").val();
                    if(value === "product"){
                        $("#categoryIdDiv").addClass("dispnone");
                        $("#productIdDiv").removeClass("dispnone");
                    }else{
                        $("#productIdDiv").addClass("dispnone");
                        $("#categoryIdDiv").removeClass("dispnone");
                    }
                }
            </script>
        </div>
    </body>
</html>
