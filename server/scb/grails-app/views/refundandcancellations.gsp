<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <asset:stylesheet src="application.css"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
    <g:set var="configs" value="${scb.Config.list()}" />
    <title>Refund and Cancellations</title>
</head>
<body style="padding:100px 200px;">
    <nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation" style="margin-bottom: 50px;border-radius: 5px;">
        <a class="navbar-brand" href="/#"><asset:image src="favicon.ico" alt="Sri Chellas Bakery"/></a>
        <h1 style="color: white;
        font-weight: bold;
        font-size: 30px;margin: 30px;">${configs.find{it.key =='companyName'}.value}</h1>
    </nav>
    
<article>

<style>
  [data-custom-class='body'], [data-custom-class='body'] * {
          background: transparent !important;
        }
[data-custom-class='title'], [data-custom-class='title'] * {
          font-family: Arial !important;
font-size: 26px !important;
color: #000000 !important;
        }
[data-custom-class='subtitle'], [data-custom-class='subtitle'] * {
          font-family: Arial !important;
color: #595959 !important;
font-size: 14px !important;
        }
[data-custom-class='heading_1'], [data-custom-class='heading_1'] * {
          font-family: Arial !important;
font-size: 19px !important;
color: #000000 !important;
        }
[data-custom-class='heading_2'], [data-custom-class='heading_2'] * {
          font-family: Arial !important;
font-size: 17px !important;
color: #000000 !important;
        }
[data-custom-class='body_text'], [data-custom-class='body_text'] * {
          color: #595959 !important;
font-size: 14px !important;
font-family: Arial !important;
        }
[data-custom-class='link'], [data-custom-class='link'] * {
          color: #3030F1 !important;
font-size: 14px !important;
font-family: Arial !important;
word-break: break-word !important;
        }
</style>

      <div data-custom-class="body">
       <div class="main_color container_wrap_first container_wrap fullsize" style=" "><div class="container"><main class="template-page content av-content-full alpha units"><div class="post-entry post-entry-type-page post-entry-688"><div class="entry-content-wrapper clearfix"><section class="av_textblock_section "><div class="avia_textblock "><p><strong>ONLINE ORDER Refund &amp; Cancellations</strong></p>

        <p><strong> <br class="avia-permanent-lb">Online Payment</strong></p>
<p>For online orders placed on <b>http://www.srichellasbakery.com</b>, the payment information, card information and bank details are entered on 3rd party website/bank website. <b>srichellasbakery.com</b> does not take any card information at any point of time from the customer. However, <b>srichellasbakery.com</b> ensures that all 3rd party links for payment have the required SSL certificate and maintain complete security of the customer data.</p>
<p>Customer can make payment through credit card, debit card, Net Banking and/or Paytm.</p>
<p>You agree to pay the price applicable for the product or service on our website at the time you submitted your order (Product Price), or any applicable taxes (defined below). You will be solely responsible for payment of all taxes, and other governmental charges.</p>

      <p><strong> <br class="avia-permanent-lb">Cancellation &amp; Refund</strong></p>
<p>For any query or cancellation of order please contact us on <b>+91 77088 07996</b> or email us on <b>srichellasbakery@gmail.com</b></p>
<p>In case the preparation of your order has started, you cannot cancel the order. There will be no refund in such an event.</p>
<p>If the cake will be in loss condition at time of delivery, after verifying by us money will be refunded.</p>
<p>Once you request the cancellation of your order, it will take time to cancel the order and initiate a refund.</p>
<p>Having the money transferred back to the source of transaction, it may take up to 7-10 business days for the respective banks to process the refund.</p>
<p>We reserve the right to cancel any order at any time without any prior notice due to uncertain conditions/occasions/disasters.</p>
        </div>
    </style>
      </div>
      </div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></article>

      </body>
</html>