package scb

class Product{

    String name
	String description    
    Double price
    Double standardKg
    ProductTag productTag
    Double discountPercentage = 0.0
    Boolean stockAvailable=true

	Date dateCreated, lastUpdated

    static belongsTo =[ category : Category]
    static mapping = {        
        sort dateCreated: 'desc'
    }

    static constraints = {
        name blank:false
        description blank:false, widget:'textarea'
        price blank:false
        standardKg nullable:true
        productTag nullable:true
        discountPercentage blank:false
        stockAvailable blank:false
    }    

    public String toString(){
        return name;
    }
}