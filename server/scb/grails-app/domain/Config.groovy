package scb

class Config {

	String key
	String value
    boolean isMandatory = false

	Date dateCreated, lastUpdated
    static constraints = {
    	key unique:true, blank: false
    	value blank: false
    }
    static mapping = {
    	key column: 'ckey'
    	value column: 'cvalue', type: 'text'
    }
}
