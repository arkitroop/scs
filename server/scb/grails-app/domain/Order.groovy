package scb

class Order{

    String name
	String mobileNumber 
	String email 
    String scbId
    String date
    String time
    String address
    String message
    String messageDeliveryStatus
    String telegramStatus

    Double weight
    Double price
    Product product
    Integer status = 0

    String razorpayOrderId
    String razorpayPaymentId
    String razorpaySignature
    String razorypayFailureResponse
    Date dateToBeDelivered
    Date dateCreated, lastUpdated

    static transients = ['paidStatus','telegramNotification']

    static mapping = {
        table 'cake_order'
        sort dateCreated: 'desc'
    	messageDeliveryStatus type: 'text'
        razorypayFailureResponse type: 'text'
    }

    static constraints = {
        scbId nullable: true
        name blank:false
        mobileNumber blank:false
        date blank:false
        time blank:false
        message blank:false
        weight blank:false
        dateToBeDelivered nullable:false

    	telegramNotification nullable: true
    	paidStatus nullable: true

        email nullable: true
        address blank:false
        price blank:false
        product nullable:true
        messageDeliveryStatus nullable:true
        telegramStatus nullable:true

        razorpayOrderId nullable:true
        razorpayPaymentId nullable:true
        razorpaySignature nullable:true
        razorypayFailureResponse nullable:true
    }    
    public getOrderedDate(){
        def date = dateCreated ? new Date(dateCreated.getTime()): new Date()
        return new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm a").format(date)
    }
    public getDeliveryDate(){
        return date+" "+time
    }
    public getWeights(){
        return weight+" Kg"
    }
    public getTelegramNotification(){
        return telegramStatus?.replace("200","Sent")
    }

    public getPaidStatus(){
        return razorpayPaymentId ? "Paid":"Unpaid"
    }
}