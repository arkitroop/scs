package scb

class Image {

	ImageSource source
	ImageEntity entity
	Long entityId
    Boolean isIcon = false

    String fileName
	String urlpath
    byte[] imageBytes 
    String imageContentType 

	Date dateCreated, lastUpdated

    static transients = ['urlpath']

    static constraints = {
        fileName nullable: false, unique:true
    	urlpath nullable: true
        imageBytes nullable: true
        imageContentType nullable: true
    }

    static mapping = {
        imageBytes column: 'image_bytes', sqlType: 'longblob' 
    }
}
