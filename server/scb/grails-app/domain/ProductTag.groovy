package scb

class ProductTag{
    String name
	
    static constraints = {
        name blank:false        
    }    

    public String toString(){
        return name;
    }
}