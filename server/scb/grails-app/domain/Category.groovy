package scb

class Category {

    String name
	String description
	Date dateCreated, lastUpdated

    static hasMany = [productList: Product]
    
    static mapping = {
        productList sort: 'stockAvailable', order: 'asc'
        sort dateCreated: 'desc'
    }

    static constraints = {
        name blank:false
        description blank:false, widget:'textarea'
    }

    public String toString(){
        return name;
    }
    public boolean stockAvailable(){
        boolean available = false
        this.productList.each{
            available = it.stockAvailable || available
        }
        return available
    }
}