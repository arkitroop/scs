package scb

import grails.util.Environment
import grails.gorm.transactions.Transactional
import groovy.json.JsonOutput
import java.net.URLEncoder
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.JsonNode
import groovy.json.JsonSlurper

@Transactional
class RazorPayUtil{

    synchronized static createOrder(order) {
        String razorpayMode = AppUtil.getConfigValue("razorpayMode")
        if(razorpayMode){
            String key = AppUtil.getConfigValue("razorpayKeyId"+razorpayMode);
            String secret = AppUtil.getConfigValue("razorpayKeySecret"+razorpayMode);
            String authtoken = AppUtil.getConfigValue("razorpayKeyAuthtoken"+razorpayMode);
            String body = '{"amount": '+(order.price*100)+',"currency": "INR","receipt": "#SCB'+order.id+'"}';
            HttpResponse<JsonNode> jsonResponse = Unirest.post("https://api.razorpay.com/v1/orders")
            .header("Authorization", "Basic "+authtoken).header("Content-Type", "application/json")
            .body(body).asJson();
            String status = jsonResponse.getStatus();
            String response = jsonResponse.getBody();
            def responseMap = (new JsonSlurper()).parseText(response)
            if(responseMap?.id){
                order.razorpayOrderId = responseMap.id;
            }
            AppUtil.save(order)
            return true
        }else{
            return false
        }
        return false
    }
}