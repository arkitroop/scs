package scb

import grails.util.Environment
import grails.gorm.transactions.Transactional
import groovy.json.JsonOutput
import java.net.URLEncoder
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.JsonNode

@Transactional
class TelegramUtil{

    synchronized static sendImage(order, params) {
        String imagePath = params.imagePath
        // String imagePath = "http://www.srichellasbakery.com/assets/dyup/lIfCGiPNXJ.jpg"
        String orderContent= "Order Number : #SCB"+order.id+"\n"+params.content+"Total Price : ₹"+order.price
        String main_response = "", main_status="";
        String encodedContent=URLEncoder.encode(orderContent, "UTF-8")
        String telegramChatList = AppUtil.getConfigValue("telegramOrderNotification");
        String telegramBotId = AppUtil.getConfigValue("telegramBotId");
        telegramChatList.split(",").each{
            String status="",response="";
            int retry = 0;
            while(!status.equals("200") && retry < 3){
                retry++;
                String url = 'https://api.telegram.org/bot'+telegramBotId+'/sendPhoto?chat_id='+it+'&photo='+imagePath+'&caption='+encodedContent;
                HttpResponse<JsonNode> jsonResponse = Unirest.get(url).asJson();
                status = jsonResponse.getStatus();
                response = jsonResponse.getBody();
                main_status += "|"+status
                main_response += "|"+response
            }
        }
        order.telegramStatus=main_status+"|"
        order.messageDeliveryStatus= main_response+"|"
        AppUtil.save(order)
    }
}