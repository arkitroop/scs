package scb

import grails.util.Environment
import grails.gorm.transactions.Transactional

@Transactional
class AppUtil{

	static current = Environment.getCurrent()
	static imageEntityClassMap = [CATEGORY: "Category",PRODUCT: "Product"]
	static randomLengthLimit = [lower: 50, upper: 100]
	static adminConfigKeys = ["adminContactNumber","adminEmailAddress","allowSMS"]
	static typeSequence = [
		0 : ('0'..'9').join(),
		1 : (('A'..'Z')+('a'..'z')).join(),
		2 : (('A'..'Z')+('a'..'z')+('0'..'9')).join()
	]

    static getRandomString(Integer type, Integer length) {
    	String firstCharacters = type == 0 ? ('1'..'9').join() : typeSequence[type]
    	String alphabets = typeSequence[type]
		def firstCharacter = firstCharacters[new Random().nextInt(firstCharacters.length()-1)]
		def	remainingCharacters = (1..length-1).collect {alphabets[new Random().nextInt(alphabets.length())] }.join()
		return firstCharacter+remainingCharacters
    }

    static getRandomOtp(Integer length){
    	// return 123456
		return Integer.parseInt(getRandomString(0,length))
	}

    static getRandomAuthtoken(){
    	int length = Math.abs( new Random().nextInt() % (randomLengthLimit.upper - randomLengthLimit.lower) ) + randomLengthLimit.lower
		return getRandomString(2, length);
    }

	static getEnv(){
		if(current == Environment.PRODUCTION){
			return "prod"
		}else if(current == Environment.TEST){
			return "test"
		}else{
			return "dev"
		}
	}
	static getConfigValue(String key){
		return Config.findByKey(key)?.value
	}

	static putConfigValue(String key, Object value){
		Config conf = Config.findByKey(key)
		conf?.value = value
		return update(conf)
	}

    static getErrorList(Object obj){
		def errorList = []
        obj.errors.allErrors.each{
			errorList << [
            	type: it.class.name,
				message: it.defaultMessage,
				arguments: it.arguments,
            	// rejectedField: it.arguments[0],
				rejectedValue: it.rejectedValue
            ]
        }
        return errorList
    }

    static delete(Object obj){
		def output = [:]
		try{
			def result = obj.validate()
			if(result){
		    	obj.delete()
		    	output = [status: true, message: "scb.delete.success", id: obj.id]			
			}else{
		    	output = [status: false, message: "scb.delete.failure", validation : false, errors : getErrorList(obj)]
			}
		}catch(Exception e){
	    	output = [status: false, message: "scb.delete.failure", error: e]
		}
		return output
	}

	static save(Object obj){
		def output = [:]
		try{
			def result = obj.validate()
			if(result){
		    	obj.save(flush:true)
		    	output = [status: true, message: "scb.save.success", id: obj.id]			
			}else{
		    	output = [status: false, message: "scb.save.failure", validation : false, errors : getErrorList(obj)]
			}
		}catch(Exception e){
	    	output = [status: false, message: "scb.save.failure", error: e]
		}
		return output
	}
}
