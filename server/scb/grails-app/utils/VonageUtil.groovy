package scb

import grails.util.Environment
import grails.gorm.transactions.Transactional
import groovy.json.JsonOutput

@Transactional
class VonageUtil{
    // static whatsappToNumberList = ["919488592710", "917708807996"]
    static whatsappToNumberList = ["919488592710","918610499889"]
    synchronized static sendWhatsAppMessage(order, content) {
        String response = "";
        whatsappToNumberList.each{
            def json = JsonOutput.toJson([
                from: "14157386102",
                to: it,
                channel:"whatsapp",
                message_type: "text", 
                text:content,
            ]);
            def process = [ 'bash', '-c', "curl -X POST https://messages-sandbox.nexmo.com/v1/messages \
            -u '522a0322:vxxfRtYSdB9Azk4U' \
            -H 'Content-Type: application/json' \
            -H 'Accept: application/json' \
            -d '${json}'" ].execute()
            
            process.waitFor()
            response+=process.err.text
            response+=process.text
        }
        return response
    }

    synchronized static sendWhatsAppImage(order, imagePath) {
        String response = "";
        whatsappToNumberList.each{
            def json = JsonOutput.toJson([
                from: "14157386102", 
                to: it, 
                channel:"whatsapp",
                message_type: "image", 
                image:[
                    url: imagePath,
                    caption: "SCB Order No. #"+order.id
                ]
            ]);

            def process = [ 'bash', '-c', "curl -X POST https://messages-sandbox.nexmo.com/v1/messages \
                -u '522a0322:vxxfRtYSdB9Azk4U' \
                -H 'Content-Type: application/json' \
                -H 'Accept: application/json' \
                -d '${json}'" ].execute()

            process.waitFor()
            response+=process.err.text
            response+=process.text
        }
        return response
    }
}


