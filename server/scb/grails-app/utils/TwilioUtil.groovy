package scb

import grails.util.Environment
import grails.gorm.transactions.Transactional
import groovy.json.JsonOutput

@Transactional
class TwilioUtil{

    synchronized static sendWhatsAppMessage(order, content) {
        String response = "";
        String whatsappToNumberList = AppUtil.getConfigValue("whatsappOrderNotification");
        whatsappToNumberList.split(",").each{
            def process = [ 'bash', '-c', 'curl -X POST https://api.twilio.com/2010-04-01/Accounts/AC0f6242063c87dec57952d4b043541c50/Messages.json \
            --data-urlencode "From=whatsapp:+14155238886" \
            --data-urlencode "Body= '+content+'" \
            --data-urlencode "To=whatsapp:'+it+'" \
            -u AC0f6242063c87dec57952d4b043541c50:1af5c1b66c99eb03b9e2041a514b745b' ].execute()
            process.waitFor()
            // response+=process.err.text
            response+=process.text
        }
        return response
    }

    synchronized static sendWhatsAppImage(order, imagePath) {
        String response = "";
        // imagePath = "http://www.srichellasbakery.com/assets/dyup/lIfCGiPNXJ.jpg"
        String whatsappToNumberList = AppUtil.getConfigValue("whatsappOrderNotification");
        whatsappToNumberList.split(",").each{
            String content = "Order Number : #SCB"+order.id
            def process = [ 'bash', '-c', 'curl -X POST https://api.twilio.com/2010-04-01/Accounts/AC0f6242063c87dec57952d4b043541c50/Messages.json \
            --data-urlencode "From=whatsapp:+14155238886" \
            --data-urlencode "Body= '+content+'" \
            --data-urlencode "To=whatsapp:'+it+'" \
            --data-urlencode "MediaUrl='+imagePath+'" \
            -u AC0f6242063c87dec57952d4b043541c50:1af5c1b66c99eb03b9e2041a514b745b' ].execute()
            process.waitFor()
            // response+=process.err.text
            response+=process.text
        }
        return response
    }
}