package scb

import grails.converters.JSON
import grails.gorm.transactions.Transactional

@Transactional
class ImageController {

	static allowedMethods = [uploadImage:"POST", uploadDirect:"POST", image:["GET","DELETE"], upload: "GET"]
    def upload(){
        render (view: 'upload', model:[categoryList: Category.list(), productList: Product.list()])
    }
    def uploadImage(){
        println ImageEntity.values()
		def input= params, output=[:]
        try {
            def file = params.file
            if(file){
	            String fullname = file.originalFilename
	            String extension = fullname.substring(fullname.lastIndexOf(".")+1)
	            if(extension.matches(/(ai|bmp|gif|ico|jpeg|jpg|png|ps|psd|svg|tif|tiff)/)){
					ImageEntity entity = ImageEntity.values().find{it.name().toLowerCase() == params.entity.toLowerCase()}
					def domain = grailsApplication.getDomainClass("scb."+AppUtil.imageEntityClassMap[entity.name()])
					if(entity && domain){
						def entityId = params.entity == "category" ? params.categoryId : params.productId
						if(domain.clazz.get(entityId)){

							String fileName = AppUtil.getRandomString(2,10)+"."+extension
							// String fileloc = AppUtil.getConfigValue(AppUtil.getEnv()+"ImageTarget")
							// File targetFile = new File(servletContext.getRealPath("/")+fileloc+fileName)
							File targetFile = new File(grailsApplication.config.getProperty('grails.var.savepath')+fileName)
							Integer imageMaxSize = Integer.parseInt(AppUtil.getConfigValue("imageMaxSize"))
							
							if(file.getSize() > imageMaxSize){
								output =[status:false, message: "scb.admin.image.upload.size.exceeded than "+(imageMaxSize/(1024))+" mb"]
							}else if(targetFile.exists()){
								output =[status:false, message: "scb.admin.image.file.exists"]
							}else{
								// Save image to webapps
								file.transferTo(targetFile)
								output = AppUtil.save(new Image(fileName: fileName, source: ImageSource.STATIC_URL, entity: entity, entityId: entityId, isIcon: false))
							}
						}else{
				            output =[status:false, message: "scb.admin.image.upload.entityid.notfound"]
						}
					}else{
			            output =[status:false, message: "scb.admin.image.upload.entity.notfound"]
					}
	            }else{
		            output =[status:false, message: "scb.admin.image.upload.noimagefound"]
	            }            	
            }else{
            	output =[status:false, message: "scb.admin.image.upload.filenotfound"]
            }
        } catch (Exception e){
            output =[status:false, message: "scb.admin.image.upload.failure", error: e.printStackTrace()]
        }
		render output as JSON
    }
	
}
