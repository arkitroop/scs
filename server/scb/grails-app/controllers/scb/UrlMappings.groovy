package scb

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "/admin"(view:"/admin")
        "/dashboard"(view:"/dashboard")
        "/policy"(view:"/policy")
        "/termsandconditions"(view:"/termsandconditions")
        "/refundandcancellations"(view:"/refundandcancellations")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
