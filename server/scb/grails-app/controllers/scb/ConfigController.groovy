package scb

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional

class ConfigController {


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Config.list(params), model:[configCount: Config.count()]
    }

    def show(Long id) {
        respond Config.get(id)
    }

    def create() {
        respond new Config(params)
    }

    @Transactional
    def save(Config config) {
        if (config == null) {
            notFound()
            return
        }

        try {
            config.save(flush:true)
        } catch (ValidationException e) {
            respond config.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'config.label', default: 'Config'), config.id])
                redirect config
            }
            '*' { respond config, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond Config.get(id)
    }

    @Transactional
    def update(Config config) {
        if (config == null) {
            notFound()
            return
        }

        try {
            config.save(flush:true)
        } catch (ValidationException e) {
            respond config.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'config.label', default: 'Config'), config.id])
                redirect config
            }
            '*'{ respond config, [status: OK] }
        }
    }

    @Transactional
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        Config.get(id).delete(flush:true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'config.label', default: 'Config'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'config.label', default: 'Config'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
