package scb

import grails.converters.JSON
import grails.gorm.transactions.Transactional

@Transactional
class AuthController {
    def login() {
    	println params
	    flash.message = ''
    	if(AppUtil.getConfigValue("masterPassword") == params.password){
    		def config = Config.findByKey("masterSessionId")
    		config.value=session.getId()
    		AppUtil.save(config)
	    	redirect(uri:"/dashboard")
    	}else{
		    flash.message = 'Invalid password, Kindly check'
	    	redirect(uri:"/admin")
    	}
    }

    def logout(){
		def config = Config.findByKey("masterSessionId")
		config.value="-"
		AppUtil.save(config)
	    flash.message = 'Logged out successfully'
    	redirect(uri:"/admin")
    }
}