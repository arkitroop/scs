package scb

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional
import grails.converters.JSON

class OrderController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");

    def index(Integer max) {
        // def todayDate = Calendar.instance
        // def startOfMonth = Calendar.instance; startOfMonth.set(Calendar.DAY_OF_MONTH, 1)
        // def endOfMonth = Calendar.instance; endOfMonth.set(Calendar.DAY_OF_MONTH, endOfMonth.getActualMaximum(Calendar.DATE))

        // def defaultOrderedDate = sdf.format(startOfMonth.time)+" - "+sdf.format(todayDate.time)
        // def defaultDeliveryDate = sdf.format(startOfMonth.time)+" - "+sdf.format(endOfMonth.time)
        def orderCriteria = Order.createCriteria()
        def orderedStartDate, orderedEndDate, deliveryStartDate, deliveryEndDate
        def orderList = [], count = 0;

        params.max = Math.min(max ?: 10, 100)
        params.orderedDate = params.orderedDate ?: "";
        params.deliveryDate = params.deliveryDate ?: "";
        params.sort = ["scbId","name","mobileNumber","message"].contains(params.sort) ? params.sort: "dateCreated"
        params.order ='desc'

        if(params.orderedDate != "" ){
            def orderedDates = params.orderedDate.split(" - ")
            orderedStartDate = sdf.parse(orderedDates[0])
            orderedEndDate = sdf.parse(orderedDates[1])
            orderedEndDate = new Date(orderedEndDate.getTime() + (1000 * 60 * 60 * 24))
        }
        if(params.deliveryDate != ""){
            def deliveryDates = params.deliveryDate.split(" - ")
            deliveryStartDate = sdf.parse(deliveryDates[0])
            deliveryEndDate = sdf.parse(deliveryDates[1])
            deliveryEndDate = new Date(deliveryEndDate.getTime() + (1000 * 60 * 60 * 24))
        }

        if(params.orderedDate != "" && params.deliveryDate != ""){
            orderList = orderCriteria.list(params){
                between("dateCreated", orderedStartDate, orderedEndDate)
                between("dateToBeDelivered", deliveryStartDate, deliveryEndDate)
            }
            count = Order.countByDateCreatedBetweenAndDateToBeDeliveredBetween(orderedStartDate,orderedEndDate,deliveryStartDate,deliveryEndDate)
        }else if(params.orderedDate != ""){
            orderList = orderCriteria.list(params){
                between("dateCreated", orderedStartDate, orderedEndDate)
            }
            count = Order.countByDateCreatedBetween(orderedStartDate,orderedEndDate)
        }else if(params.deliveryDate != ""){
            orderList = orderCriteria.list(params){
                between("dateToBeDelivered", deliveryStartDate, deliveryEndDate)
            }
            count = Order.countByDateToBeDeliveredBetween(deliveryStartDate, deliveryEndDate)
        }else{
            orderList = orderCriteria.list(params){}
            count = Order.count()
        }
        respond orderList, model:[orderCount: count, orderedDate: params.orderedDate, deliveryDate: params.deliveryDate]
    }

    def show(Long id) {
        def savepath = grailsApplication.config.getProperty('grails.var.savepath')
        def servepath = grailsApplication.config.getProperty('grails.var.servepath')
        def order = Order.get(id)
        def imgpath = ""
        if(order.product != null){
            def imgList = Image.findAllByEntityAndEntityId(scb.ImageEntity.PRODUCT,order.product.id).reverse()
            def img = imgList.find{new File(savepath+it?.fileName).exists() == true}
            imgpath = servepath+img?.fileName
        }

        respond order, model:[imgpath: imgpath]
    }

    @Transactional
    def save(){
    	def output=[status: true]
    	Product product = Product.get(params.productId)

    	Order order = new Order(params)
    	order.product = product
    	order.price = (product.standardKg != null ? 1 : Double.parseDouble(params.weight+''))*product.price*((100 - product.discountPercentage)/100)
        order.dateToBeDelivered = (new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm a")).parse(params.date+" "+params.time)
    	AppUtil.save(order)
        order.scbId = "#SCB"+order.id
    	output= AppUtil.save(order)

        // send telegram msg
        // TelegramUtil.sendImage(order, params)
        boolean status = RazorPayUtil.createOrder(order)
        if(status){
            String razorpayMode = AppUtil.getConfigValue("razorpayMode")
            String key = AppUtil.getConfigValue("razorpayKeyId"+razorpayMode);
            output.order_id=order.razorpayOrderId
            output.razor_key=key
            output.amount=order.price*100
        }
    	render output as JSON
    }

    @Transactional
    def checkoutOrder(){
    	def output=[status: false]
        Order order = Order.findByRazorpayOrderId(params.razorpay_order_id)
        String razorpayMode = AppUtil.getConfigValue("razorpayMode")
        String secret = AppUtil.getConfigValue("razorpayKeySecret"+razorpayMode)
        String generated_signature = HmacUtil.generateHmac256(params.razorpay_order_id + "|" + params.razorpay_payment_id, secret.getBytes())

        if (generated_signature == params.razorpay_signature) {
            order.razorpayOrderId=params.razorpay_order_id
            order.razorpayPaymentId=params.razorpay_payment_id
            order.razorpaySignature=params.razorpay_signature
            AppUtil.save(order)
            TelegramUtil.sendImage(order, params)
            output.status = true
        }
    	render output as JSON
    }

    @Transactional
    def paymentFailure(){
    	def output=[status: true]
        Order order = Order.findByRazorpayOrderId(params.razorpay_order_id)
        order.razorypayFailureResponse = params.failure_response
        AppUtil.save(order)
        output.status = true
    	render output as JSON
    }

    @Transactional
    def updateStatus(){    	
    	def output=[status: true]
    	def order = Order.get(Long.parseLong(params.id))
    	order.status = Integer.parseInt(params.status)
    	output = AppUtil.save(order)
    	render output as JSON
    }
}