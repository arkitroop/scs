package scb

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import grails.gorm.transactions.Transactional
import grails.converters.JSON

class CategoryController {


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", stockUpdate:"POST"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Category.list(params), model:[categoryCount: Category.count()]
    }

    def show(Long id) {
        respond Category.get(id)
    }

    def create() {
        respond new Category(params)
    }

    @Transactional
    def save(Category category) {
        if (category == null) {
            notFound()
            return
        }

        try {
             category.save()
        } catch (ValidationException e) {
            respond category.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'category.label', default: 'Category'), category.id])
                redirect category
            }
            '*' { respond category, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond Category.get(id)
    }

    @Transactional
    def update(Category category) {
        if (category == null) {
            notFound()
            return
        }

        try {
            category.save(flush:true)
            def tempPrice = Double.parseDouble(params?.price)
            if(tempPrice > 0){
                category.productList?.each{
                    it.price = tempPrice
                    it.save(flush:true)
                }
            }

        } catch (ValidationException e) {
            respond category.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'category.label', default: 'Category'), category.id])
                redirect category
            }
            '*'{ respond category, [status: OK] }
        }
    }
    
    @Transactional
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        Category.get(id).delete(flush:true)
        
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'category.label', default: 'Category'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'category.label', default: 'Category'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def stockup(){}

    @Transactional
    def stockUpdate(){
        def output = [status: true, message: "scb.stock.update.success"]
        try{
            Product.getAll(params["in[]"]).each{it->
                it.stockAvailable = true
                it.save(flush:true)
            }
            Product.getAll(params["out[]"]).each{it->
                it.stockAvailable = false
                it.save(flush:true)
            }            
        }catch(Exception e){
            output.status = false
        }
        render output as JSON
    }

    def showProducts(Long id) {
        def category = Category.get(id);
        respond category, model : [ categoryName : category.name, products : Product.findAllByCategoryAndStockAvailable(category,true)]        
    }
}
