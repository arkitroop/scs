package scb

import grails.converters.JSON

class LoginInterceptor {

	LoginInterceptor() {
        matchAll()
    }

    boolean before() {
    	def output = [:], authorized = false, admin = false
		String sessionId = session.getId()
		if(sessionId != null){
			admin = AppUtil.getConfigValue("masterSessionId") == sessionId
		}
		if(admin || (["", "admin", "auth", "favicon", "category", "order", "policy", "termsandconditions", "refundandcancellations", null].contains(controllerName) && ["login", "logout", "showProducts", "save", "checkoutOrder", "paymentFailure", null].contains(actionName) )){
			// if(controllerName == null || controllerName == ""){
			// 	def config = Config.findByKey("websiteHit")
			// 	config.value=Long.parseLong(config.value)+1
			// 	println config.value
			// 	config.save()
			// }
			if(controllerName == "admin" && admin){
				redirect(uri:"/dashboard")
			}
			authorized = true
		}else{
			if(["product","category","config","image", "dashboard","order"].contains(controllerName)){
	            redirect(controller : "admin")
			}else{
			    redirect(uri:"/")
			}
		}
		return authorized
    }

    boolean after() { 
    	true
    }

    void afterView() {
    }
    
}
